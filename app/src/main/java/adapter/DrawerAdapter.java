package adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import data.tables.SubTabs;
import dev.aboriginal.Models.TabModel;
import dev.aboriginal.OnResult;
import dev.aboriginal.R;
import dev.aboriginal.ui.ChannelListActivity;
import dev.aboriginal.ui.LoginActivity;
import dev.aboriginal.ui.PdfActivity;
import dev.aboriginal.ui.PolicyActivity;
import dev.aboriginal.ui.PubNubChatActivity;
import dev.aboriginal.utils.AppPreferences;
import dev.aboriginal.utils.PreferenceConnector;

/**
 * Created by Android on 11/10/2017.
 */

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {
    private final AppPreferences appPreferences;
    AlertDialog.Builder builder;
    Context context;

    OnResult onResult;
    ArrayList<TabModel> upperTabs;
    ArrayList<TabModel> subTabs = new ArrayList<>();
    ArrayList<TabModel> subTabsTemp = new ArrayList<>();
    ArrayList<Boolean> isOpened = new ArrayList<>();

    public DrawerAdapter(Context context, ArrayList<TabModel> upperTabs, OnResult onResult) {
        this.context = context;
        this.upperTabs = upperTabs;
        this.onResult = onResult;
        for (TabModel tabModel : upperTabs) {
            isOpened.add(true);
        }
        appPreferences = new AppPreferences(context);
    }

    @Override
    public @NotNull ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drawer_items, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final @NotNull ViewHolder holder, final int position) {

        if (upperTabs.get(position).getSize().equalsIgnoreCase("0")) {

            holder.arrow_image.setVisibility(View.GONE);
        }
        if (position == 0) {

            holder.upper_items_drawer.setVisibility(View.VISIBLE);

        } else {
            holder.upper_items_drawer.setVisibility(View.GONE);

        }


        if (position == upperTabs.size() - 1) {

            Log.i("kkk", "pos-- " + position);
            Log.i("kkk", "uper-- " + upperTabs.size());

            holder.policy_lay.setVisibility(View.VISIBLE);
            holder.view_policy.setVisibility(View.VISIBLE);
            holder.chat_lay.setVisibility(View.VISIBLE);
            holder.view_chat.setVisibility(View.VISIBLE);
            holder.workbook_lay.setVisibility(View.VISIBLE);
            holder.view_work.setVisibility(View.VISIBLE);

            if (PreferenceConnector.getInstance(context).loadSavedPreferences("isLogin", "").equalsIgnoreCase("")) {
                holder.logout_lay.setVisibility(View.GONE);
            } else {
                holder.logout_lay.setVisibility(View.VISIBLE);
            }
        } else {
            holder.chat_lay.setVisibility(View.GONE);
            holder.view_chat.setVisibility(View.GONE);
            holder.workbook_lay.setVisibility(View.GONE);
            holder.view_work.setVisibility(View.GONE);
            holder.policy_lay.setVisibility(View.GONE);
            holder.view_policy.setVisibility(View.GONE);
            holder.logout_lay.setVisibility(View.GONE);
        }

        holder.policy_lay.setOnClickListener(v -> {
            Intent i = new Intent(context, PolicyActivity.class);
            i.putExtra("comingfrom", "drawer");
            context.startActivity(i);
        });


        holder.logout_lay.setOnClickListener(v -> {

            builder = new AlertDialog.Builder(context);

            //Uncomment the below code to Set the message and title from the strings.xml file
            builder.setMessage("Do you really want to logout").setTitle("Logout");

            //Setting message manually and performing action on button click
            builder.setMessage("Do you really want to logout?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", (dialog, id) -> {
                        Intent i = new Intent(context, LoginActivity.class);
                        PreferenceConnector.getInstance(context).savePreferences("isLogin", "");
                        context.startActivity(i);
                    })
                    .setNegativeButton("No", (dialog, id) -> {
                        //  Action for 'NO' Button
                        dialog.cancel();

                    });
            //Creating dialog box
            AlertDialog alert = builder.create();
            //Setting the title manually
            alert.setTitle("Logout");
            alert.show();


        });

        holder.workbook_lay.setOnClickListener(v -> {
            Intent i = new Intent(context, PdfActivity.class);
            context.startActivity(i);

        });

        holder.chat_lay.setOnClickListener(v -> {

            if (PreferenceConnector.getInstance(context).loadSavedPreferences("isLogin", "").equalsIgnoreCase("")) {
                // holder.logout_lay.setVisibility(View.GONE);
                Intent i = new Intent(context, LoginActivity.class);
                context.startActivity(i);
            } else {
                // holder.logout_lay.setVisibility(View.VISIBLE);
//                    Intent i = new Intent(context, ChatActivity_new.class);

                if (appPreferences.getuser_is_admin()) {
                    Intent i = new Intent(context, ChannelListActivity.class);
                    context.startActivity(i);
                } else {
                    Intent i = new Intent(context, PubNubChatActivity.class);
                    context.startActivity(i);
                }
            }
        });

        holder.textView.setText(upperTabs.get(position).getTabName());

        holder.back_button.setOnClickListener(view -> onResult.onResult(1111));

        holder.full_lay.setOnClickListener(view -> {

            onResult.onResult(position);

            subTabs = SubTabs.getInstance().getSizes();
            subTabsTemp.clear();
            for (int i = 0; i < subTabs.size(); i++) {

                if (upperTabs.get(position).getSorting().equalsIgnoreCase(subTabs.get(i).getId())) {

                    subTabsTemp.add(subTabs.get(i));
                }

            }
            if (!subTabsTemp.isEmpty()) {
                if (isOpened.get(position)) {
                    holder.arrow_image.setRotation(90);

                    holder.subTabs_recycler_view.setVisibility(View.VISIBLE);
                    SubTabsDrawerAdapter subTabsDrawerAdapter = new SubTabsDrawerAdapter(context, subTabsTemp, onResult, position);
                    holder.subTabs_recycler_view.setAdapter(subTabsDrawerAdapter);
                    isOpened.add(position, false);
                } else {
                    holder.arrow_image.setRotation(0);
                    holder.subTabs_recycler_view.setVisibility(View.GONE);
                    isOpened.add(position, true);

                }
//                   holder.subTabs_recycler_view.setVisibility(View.VISIBLE);

            } else {

                onResult.onResult(position);
            }


        });
    }

    @Override
    public int getItemCount() {
        // int size=upperTabs.size()+1;
        return upperTabs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout upper_items_drawer, full_lay, policy_lay, logout_lay, workbook_lay, chat_lay;
        TextView textView;
        ImageView arrow_image, back_button;
        RecyclerView subTabs_recycler_view;
        View view_work, view_chat, view_policy;

        public ViewHolder(View itemView) {
            super(itemView);
            view_work = itemView.findViewById(R.id.view_work);
            view_chat = itemView.findViewById(R.id.view_chat);
            view_policy = itemView.findViewById(R.id.view_policy);
            textView = itemView.findViewById(R.id.title);
            arrow_image = itemView.findViewById(R.id.arrow_image);
            back_button = itemView.findViewById(R.id.back_button);
            upper_items_drawer = itemView.findViewById(R.id.upper_items_drawer);
            full_lay = itemView.findViewById(R.id.full_lay);
            workbook_lay = itemView.findViewById(R.id.workbook_lay);
            chat_lay = itemView.findViewById(R.id.chat_lay);
            policy_lay = itemView.findViewById(R.id.policy_lay);
            logout_lay = itemView.findViewById(R.id.logout_lay);
            subTabs_recycler_view = itemView.findViewById(R.id.subTabs_recycler_view);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            subTabs_recycler_view.setLayoutManager(mLayoutManager);
        }
    }
}
