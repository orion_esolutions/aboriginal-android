package adapter;

import dev.aboriginal.Models.TabModel;
import dev.aboriginal.OnResult;
import dev.aboriginal.R;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Android on 11/10/2017.
 */

public class SubTabsDrawerAdapter extends RecyclerView.Adapter<SubTabsDrawerAdapter.ViewHolder>  {

    Context context;
    ArrayList<TabModel> subTabs=new ArrayList<>();
    OnResult onResult;
    int pos;

    public SubTabsDrawerAdapter(Context context, ArrayList<TabModel> subTabs, OnResult onResult,int pos) {
        this.onResult=onResult;
        this.context = context;
        this.subTabs = subTabs;
        this.pos=pos;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_tabs_drawer_items, parent, false);

        return new ViewHolder(itemView);
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.textView.setText(subTabs.get(position).getTabName());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResult.onResult(position,"subTab",pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subTabs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        public ViewHolder(View itemView) {
            super(itemView);
            textView=(TextView)itemView.findViewById(R.id.title);
        }
    }
}
