package datasource;

import dev.aboriginal.Models.TabModel;
import dev.aboriginal.R;
import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import data.tables.SubTabs;
import data.tables.UpperTabs;

/**
 * Created by msahakyan on 22/10/15.
 */
public class ExpandableListDataSource {

    /**
     * Returns fake data of films
     *
     * @param context     * @return
     */
    public static Map<String, List<String>> getData(Context context) {
        Map<String, List<String>> expandableListData = new TreeMap<>();

        ArrayList<TabModel> subTabs=new ArrayList<>();
        List<String> filmGenres = Arrays.asList(context.getResources().getStringArray(R.array.film_genre));

        ArrayList<TabModel> upperTabs= UpperTabs.getInstance().getSizes();

        List<String> musicalFilms = Arrays.asList(context.getResources().getStringArray(R.array.musicals));
        List<String> dramaFilms = Arrays.asList(context.getResources().getStringArray(R.array.dramas));
        List<String> thrillerFilms = Arrays.asList(context.getResources().getStringArray(R.array.thrillers));
        List<String> comedyFilms = Arrays.asList(context.getResources().getStringArray(R.array.comedies));
        for(int i=0;i<upperTabs.size();i++){
            List<String> actionFilms = new ArrayList<>();
            subTabs=  SubTabs.getInstance().getSizes();
            for(int j=0;j<subTabs.size();j++){
                if(upperTabs.get(i).getSorting().equalsIgnoreCase(subTabs.get(j).getId())){
                    actionFilms.add(subTabs.get(j).getTabName());

                }

            }

            expandableListData.put(upperTabs.get(i).getSorting(), actionFilms);
        }




        return expandableListData;
    }
}
