package fragment;

import dev.aboriginal.Models.DrawerModel;
import dev.aboriginal.Models.TabModel;
import dev.aboriginal.OnResult;
import dev.aboriginal.R;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import org.sufficientlysecure.htmltextview.HtmlResImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import data.tables.SubTabs;
import data.tables.UpperTabs;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentAction#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAction extends Fragment implements OnResult {
    ArrayList<TabModel> upperTabs = new ArrayList<>();
    ArrayList<TabModel> subTabs = new ArrayList<>();
    ArrayList<TabModel> tempSubTabs = new ArrayList<>();
    private static final String KEY_MOVIE_TITLE = "key_title";
    private static final String POSITION = "pos";
    private static final String UPPER_POSITION = "upper_pos";
    private static final String HIGHLIGHT_TEXT = "highlight_text";
    String posiiton, upperPos, hightlightedText;
    HtmlTextView textView;

    ScrollView scrollView;

    public FragmentAction() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment FragmentAction.
     */
    public static FragmentAction newInstance(String movieTitle, String pos, String upperPos, String highlightText) {
        FragmentAction fragmentAction = new FragmentAction();
        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        args.putString(POSITION, pos);
        args.putString(UPPER_POSITION, upperPos);
        args.putString(HIGHLIGHT_TEXT, highlightText);

        fragmentAction.setArguments(args);

        return fragmentAction;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_action, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        posiiton = getArguments().getString(POSITION);
        upperPos = getArguments().getString(UPPER_POSITION);
        hightlightedText = getArguments().getString(HIGHLIGHT_TEXT);


        scrollView = (ScrollView) view.findViewById(R.id.scroll_view);

        OverScrollDecoratorHelper.setUpOverScroll(scrollView);
        ImageView staffNotes = ((ImageView) view.findViewById(R.id.staff_notes));
        textView = (HtmlTextView) view.findViewById(R.id.movie_title);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Roboto-Regular.ttf");
        textView.setTypeface(tf);
        upperTabs = UpperTabs.getInstance().getSizes();

        if (!upperTabs.isEmpty()) {
            Collections.sort(upperTabs, new Comparator<TabModel>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public int compare(TabModel fruit2, TabModel fruit1) {
                    int i = Integer.parseInt(fruit1.getSorting());
                    int j = Integer.parseInt(fruit2.getSorting());
                    return Integer.compare(i, j);
                }
            });
            Collections.reverse(upperTabs);
            if (upperTabs.get(Integer.parseInt(upperPos)).getStaffNotes().equalsIgnoreCase("")) {

                staffNotes.setVisibility(View.GONE);
            }

        }

        staffNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog alertDialog = new Dialog(getActivity());
                alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                LinearLayout.LayoutParams dialogParams = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, 800);
                LayoutInflater inflater = getLayoutInflater();
                View convertView = (View) inflater.inflate(R.layout.custom_dialog_box, null);
                alertDialog.setContentView(convertView, dialogParams);
                alertDialog.setTitle("Staff Notes");

                Button add_new_group = (Button) convertView.findViewById(R.id.add_new_group);
                Button header_text = (Button) convertView.findViewById(R.id.header_text);
                HtmlTextView no_record = (HtmlTextView) convertView.findViewById(R.id.no_record);
                Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Roboto-Regular.ttf");
                no_record.setTypeface(tf);
                add_new_group.setTypeface(tf);
                header_text.setTypeface(tf);
                String notes = upperTabs.get(Integer.parseInt(upperPos)).getStaffNotes().trim();
                notes = notes.replace("<li>", "<br/> <img src=\"dot\"/>&nbsp;&nbsp;&nbsp;");
                notes = notes.replace("<ul>", "");
                notes = notes.replace("</li>", "<br/>");
                notes = notes.replace("</ul>", "");
                no_record.setHtml(notes, new HtmlResImageGetter(no_record));
//                no_record.setHtml(notes);
                add_new_group.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();

                    }
                });

                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();

            }
        });


        subTabs = SubTabs.getInstance().getSizes();

        for (TabModel tabModel : subTabs) {

            if (upperTabs.get(Integer.parseInt(upperPos)).getSorting().equalsIgnoreCase(tabModel.getId())) {

                tempSubTabs.add(tabModel);

            }
        }

        if (hightlightedText.equalsIgnoreCase("Scroll")) {
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
                    int indexOfWord = textView.getText().toString().indexOf(tempSubTabs.get(Integer.parseInt(posiiton)).getTabName());
                    int line = textView.getLayout().getLineForOffset(indexOfWord);
                    int y = textView.getLayout().getLineTop(line);
                    scrollView.scrollTo(0, y);
//                        }
//                    }, 1000);

                }
            });

        }

        String content = "";
        if (!subTabs.isEmpty()) {

            if (!hightlightedText.equalsIgnoreCase("")) {

                StringBuilder stringBuilder = new StringBuilder();


                for (int index = 0; index < tempSubTabs.size(); index++) {
                    String description = tempSubTabs.get(index).getDescription();
                    String tags = "<br/>";
                    if (description.contains("<ul>"))
                        tags = "<br> &nbsp;";


                    content = content + "<br>  <font color='#9e3323'><b>" + tempSubTabs.get(index).getTabName().trim() + "</b></font>" + tags + description;
                    stringBuilder.append(tempSubTabs.get(index).getTabName().trim() + "\n" + tempSubTabs.get(index).getDescription());

                }
//                SpannableString ss = new SpannableString("abc");
//                Drawable d = getResources().getDrawable(R.drawable.icon32);
//                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
//                ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
//                ss.setSpan(span, 0, 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

                if (!tempSubTabs.isEmpty()){
                    if (!tempSubTabs.get(Integer.parseInt(posiiton)).getDescription().equalsIgnoreCase("")) {

                        content = content.replace("<li>", "<br/> <img src=\"dot\"/>&nbsp;&nbsp;&nbsp;");
                        content = content.replace("<ul>", "");
                        content = content.replace("</li>", "<br/>");
                        content = content.replace("</ul>", "");
                        textView.setHtml(content, new HtmlResImageGetter(textView));
                        highlightString(hightlightedText);
//

                    } else {
                        textView.setText("No Content Found");

                    }
                }
                else {
                    textView.setText("No Content Found");

                }

            } else {
                if (!tempSubTabs.isEmpty()) {
                    if (!tempSubTabs.get(Integer.parseInt(posiiton)).getDescription().equalsIgnoreCase("")) {
                        StringBuilder stringBuilder = new StringBuilder();

                        for (int index = 0; index < tempSubTabs.size(); index++) {

                            String description = tempSubTabs.get(index).getDescription();
                            String tags = "<br/>";
                            if (description.contains("<ul>"))
                                tags = "<br> &nbsp;";


                            content = content + "<br>  <font color='#9e3323'><b>" + tempSubTabs.get(index).getTabName().trim() + "</b></font>" + tags + description;
                            stringBuilder.append(tempSubTabs.get(index).getTabName() + "\n" + tempSubTabs.get(index).getDescription());

                        }

                        content = content.replace("<li>", "<br/> <img src=\"dot\"/>&nbsp;&nbsp;&nbsp;");
                        content = content.replace("<ul>", "");
                        content = content.replace("</li>", "<br/>");
                        content = content.replace("</ul>", "");
                        textView.setHtml(content, new HtmlResImageGetter(textView));
//

                    } else {
                        textView.setText("No Content Found");

                    }
                } else {
                    textView.setText("No Content Found");
                }


            }

        }

    }

    public SpannableString textToImage(String content, Context c) {
        SpannableString ss = new SpannableString(content);
        int starts = 0;
        int end = 0;
        if (content.indexOf("[", starts) != -1 && content.indexOf("]", end) != -1) {
            starts = content.indexOf("[", starts);
            end = content.indexOf("]", end);
            SharedPreferences shared = c.getSharedPreferences("emotion", 0);
            int resource = shared.getInt(content, 0);
            try {
                Drawable drawable = c.getResources().getDrawable(resource);
                if (drawable != null) {
                    drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                    ImageSpan span = new ImageSpan(drawable, ImageSpan.ALIGN_BASELINE);
                    ss.setSpan(span, starts, end + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            } catch (Exception ex) {

            }
        }
        return ss;
    }

    private void highlightString(String input) {
//Get the text from text view and create a spannable string
        SpannableString spannableString = new SpannableString(textView.getText());
//Get the previous spans and remove them
        BackgroundColorSpan[] backgroundSpans = spannableString.getSpans(0, spannableString.length(), BackgroundColorSpan.class);

        for (BackgroundColorSpan span : backgroundSpans) {
            spannableString.removeSpan(span);
        }

//Search for all occurrences of the keyword in the string
        int indexOfKeyword = spannableString.toString().toLowerCase().indexOf(input.toLowerCase());

        while (indexOfKeyword > 0) {
            //Create a background color span on the keyword
            spannableString.setSpan(new BackgroundColorSpan(Color.YELLOW), indexOfKeyword, indexOfKeyword + input.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            //Get the next index of the keyword
            indexOfKeyword = spannableString.toString().toLowerCase().indexOf(input.toLowerCase(), indexOfKeyword + input.length());
        }

//Set the final text on TextView
        textView.setText(spannableString);
    }

    @Override
    public void onResult(DrawerModel drawerModel) {

    }

    @Override
    public void onResult(final String textToHighlight) {

    }

    @Override
    public void onResult(int pos) {


    }

    @Override
    public void onResult(int pos, String subtab, int upperPos) {

    }


    private class ImageGetter implements Html.ImageGetter {
        public Drawable getDrawable(String source) {
            int id;
            if (source.equals("hughjackman.jpg")) {
                id = R.drawable.app_icon;
            } else {
                return null;
            }
            Drawable d = getResources().getDrawable(id);
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
            return d;
        }
    }

    ;

}
