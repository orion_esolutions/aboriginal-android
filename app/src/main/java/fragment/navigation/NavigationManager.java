package fragment.navigation;

/**
 * @author msahakyan
 */

public interface NavigationManager {

    void showFragmentAction(String title,String pos,String upperPos,String highlightText);

    void showFragmentComedy(String title);

    void showFragmentDrama(String title);

    void showFragmentMusical(String title);

    void showFragmentThriller(String title);
}
