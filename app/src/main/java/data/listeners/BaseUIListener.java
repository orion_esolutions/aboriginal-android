
package data.listeners;

/**
 * Base listener to notify UI about some changes.
 * 
 * This listener should be registered from {@link android.app.Activity#onResume()} and
 * unregistered from {@link android.app.Activity#onPause()}.
 * 
 * @author alexander.ivanov
 * 
 */
public interface BaseUIListener {

}
