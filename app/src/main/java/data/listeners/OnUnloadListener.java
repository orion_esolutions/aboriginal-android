package data.listeners;


import data.BaseManagerInterface;

/**
 * Listen for application to being closed.
 * 
 * @author alexander.ivanov
 * 
 */
public interface OnUnloadListener extends BaseManagerInterface {

	/**
	 * Called before application to be killed after
	 * {@link com.brainzter.itsme.listeners.OnCloseListener#onClose()} has been called.
	 * 
	 * WILL BE CALLED FROM BACKGROUND THREAD. DON'T CHANGE OR ACCESS
	 * APPLICATION'S DATA HERE!
	 */
	public void onUnload();

}
