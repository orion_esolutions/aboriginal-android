package data.listeners;


import data.BaseManagerInterface;

/**
 * Listener for the periodically call.
 * 
 * At least {@link #DELAY} milliseconds will pass before the next call.
 * 
 * First time it will be called after at least {@link #DELAY} milliseconds after
 * {@link OnInitializedListener#onInitialized()}.
 * 
 * @author alexander.ivanov
 * 
 */
public interface OnTimerListener extends BaseManagerInterface {

	public final static int DELAY = 1000;

	/**
	 * Called after at least {@link #DELAY} milliseconds.
	 */
	void onTimer();

}
