package data.tables;

import dev.aboriginal.Models.TabModel;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;

import data.AbstractTable;
import data.DatabaseManager;

/**
 * Created by Android on 9/19/2017.
 */

public class UpperTabs extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {
        }

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String PATH = "path";
        public static final String SIZE = "size";
        public static final String SORTING = "sorting";
        public static final String DATE_CREATED = "date_created";
        public static final String DATE_MODIFIED = "date_modified";


    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "upperTabs";

    private final static UpperTabs instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new UpperTabs(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static UpperTabs getInstance() {

        return instance;
    }

    private UpperTabs(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }


    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.NAME,
            Fields.PATH,
            Fields.SIZE,
            Fields.DATE_CREATED,
            Fields.DATE_MODIFIED,

    };


    public Long write(

            String tabname,
            String staffnotes,
            int sorting,
            String size
    ) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.NAME, tabname);
        mContentValues.put(Fields.PATH, staffnotes);
        mContentValues.put(Fields.SORTING, sorting);
        mContentValues.put(Fields.SIZE, size);



        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


    public void updateUnits( String tabname,
                             String staffnotes,
                             String sorting,
                             String size

    ) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.NAME, tabname);
        mContentValues.put(Fields.PATH, staffnotes);
        mContentValues.put(Fields.SORTING, sorting);
        mContentValues.put(Fields.SIZE, size);

        openDatabase();
        db.update(NAME, mContentValues, Fields.SORTING + " = ?", new String[]{sorting});
    }


//	public void updateMessageImageStatus(Long messageId,int status){
//
//
//		ContentValues mContentValues  = new ContentValues();
//
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//
//	}


    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE if not exists  " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.NAME + " TEXT,"
                + Fields.PATH + " TEXT,"
                + Fields.SIZE + " TEXT,"
                + Fields.SORTING + " TEXT,"
                + Fields.DATE_CREATED + " DATETIME,"
                + Fields.DATE_MODIFIED + " TEXT"
                +
                ");";
//        String sql = "CREATE TABLE if not exists  " + NAME + " ("
//                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
//                + Fields.SIZE_UNIT_ID + " INTEGER,"
//                + Fields.SERVER_ID + " TEXT,"
//                + Fields.STANDARD_UNIT_ID + " TEXT,"
//                + Fields.STATE_ID + " INTEGER,"
//                + Fields.SIZE_UNIT_VALUE + " INTEGER,"
//                + Fields.UNIT_NAME + " TEXT,"
//                + Fields.CREATED + " DATETIME, "
//                + Fields.MODIFIED + " DATETIME" +
//                ");";

        DatabaseManager.execSQL(db, sql);

    }


    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }
    public void deleteRow(String showId) {
        openDatabase();

        String mSelectQuery = "Delete from " + NAME+" where "+Fields.ID+" == "+showId;

        db.execSQL(mSelectQuery);

        closeDatabase();
    }
    public boolean getSizeUnit(int serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.NAME + "= " + serverId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;


        return false;
    }


//    public String readCategories(int sizeUnitId, int stateId) {
//
//        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
//                Fields.SIZE_UNIT_ID + "=" + sizeUnitId + " and " + Fields.STATE_ID + "=" + stateId,
//                null, null, null, null);
//
//        String size = "";
//
//        if (cursor != null && cursor.moveToFirst()) {
//            while (!cursor.isAfterLast()) {
//
//                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
//                //messageModeList.add(generateObjectFromCursor(cursor));
//                size = generateObjectFromCursor(cursor).getSizeUnitValue();
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//        return size;
//    }

    public ArrayList<TabModel> getSizes() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, Fields.SORTING, null, null);

        ArrayList<TabModel> sizeUnitModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

//                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
                //messageModeList.add(generateObjectFromCursor(cursor));
                sizeUnitModels.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }

            cursor.close();
        }

        return sizeUnitModels;
    }

    public TabModel  getSizeUnitModel(int sizeUnitId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.NAME + "=" + sizeUnitId,
                null, null, null, null);

        TabModel size = new TabModel();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                Log.e("Size unit value", generateObjectFromCursor(cursor).getTabName());
                //messageModeList.add(generateObjectFromCursor(cursor));
                size = generateObjectFromCursor(cursor);

                cursor.moveToNext();
            }
            cursor.close();
        }

        return size;

    }

    public String checkState(int stateId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.NAME + "=" + stateId,
                null, null, null, null);

        String size = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                Log.e("Size unit value", generateObjectFromCursor(cursor).getTabName());
                //messageModeList.add(generateObjectFromCursor(cursor));
                size = generateObjectFromCursor(cursor).getTabName();
                cursor.moveToNext();
            }
            cursor.close();
        }
        return size;
    }


    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.NAME,
                Fields.PATH,
                Fields.SORTING,
                Fields.SIZE,


        };
    }


    public TabModel generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        TabModel categoryModel = new TabModel();



        categoryModel.setId(cursor.getString(cursor.getColumnIndex(Fields.ID)));
        categoryModel.setTabName(cursor.getString(cursor.getColumnIndex(Fields.NAME)));
        categoryModel.setSorting(cursor.getString(cursor.getColumnIndex(Fields.SORTING)));
        categoryModel.setStaffNotes(cursor.getString(cursor.getColumnIndex(Fields.PATH)));
        categoryModel.setSize(cursor.getString(cursor.getColumnIndex(Fields.SIZE)));



        return categoryModel;
    }

}
