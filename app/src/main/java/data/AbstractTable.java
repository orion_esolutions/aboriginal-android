
package data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Abstract database table.
 * 
 * @author alexander.ivanov
 * 
 */
public abstract class AbstractTable implements DatabaseTable {

	protected abstract String getTableName();

	protected abstract String[] getProjection();

	protected String getListOrder() {
		return null;
	}

	@Override
	public void migrate(SQLiteDatabase db, int toVersion)
	{
		
	}
	
	/**
	 * Query table.
	 * 
	 * @return Result set with defined projection and in defined order.
	 */
	public Cursor list() {
		SQLiteDatabase db = DatabaseManager.getInstance().getReadableDatabase();
		
		
		Log.e("TAGGER", getTableName()+"  "+getProjection()+"  "+getListOrder());
		
		return db.query(getTableName(), getProjection(), null, null, null,null, getListOrder());
	}

	@Override
	public void clear() {
		SQLiteDatabase db = DatabaseManager.getInstance().getWritableDatabase();
		db.delete(getTableName(), null, null);
	}
	


}
