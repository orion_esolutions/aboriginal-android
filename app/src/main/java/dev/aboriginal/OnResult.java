package dev.aboriginal;

import dev.aboriginal.Models.DrawerModel;

/**
 * Created by Android on 11/8/2017.
 */

public interface OnResult {

    public void onResult(DrawerModel drawerModel);
    public void onResult(String  textToHighlight);
    public void onResult(int  pos);
    public void onResult(int  pos,String subtab,int upperPos);

}
