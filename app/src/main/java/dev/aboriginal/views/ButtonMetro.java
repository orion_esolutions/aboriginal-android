package dev.aboriginal.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;

/**
 * Created by Devteam2 on 6/14/2017.
 */

public class ButtonMetro extends AppCompatButton {

    public ButtonMetro(Context context) {
        super(context);
    }

    public ButtonMetro(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(this, context, attrs);
    }

    public ButtonMetro(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(this, context, attrs);
    }

    /**
     * Sets a font on a textview based on the custom com.my.package:font attribute
     * If the custom font attribute isn't found in the attributes nothing happens
     * @param textview
     * @param context
     * @param attrs
     */
    public void setCustomFont(TextView textview, Context context, AttributeSet attrs) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/metropolis_medium.otf");
        setTypeface(tf);
    }

    /**
     * Sets a font on a textview
     * @param textview
     * @param font
     * @param context
     */
    public void setCustomFont(TextView textview, String font, Context context) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/metropolis_medium.otf");
        setTypeface(tf);

    }
}
