package dev.aboriginal.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;


/**
 * Created by Devteam2 on 6/14/2017.
 */

public class TextViewRegular extends AppCompatTextView {

    public TextViewRegular(Context context) {
        super(context);
    }

    public TextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(this, context, attrs);
    }

    public TextViewRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(this, context, attrs);
    }

    /**
     * Sets a font on a textview based on the custom com.my.package:font attribute
     * If the custom font attribute isn't found in the attributes nothing happens
     * @param textview
     * @param context
     * @param attrs
     */
    public void setCustomFont(TextView textview, Context context, AttributeSet attrs) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/sourcesanspro_regular.ttf");
        setTypeface(tf);
    }

    /**
     * Sets a font on a textview
     * @param textview
     * @param font
     * @param context
     */
    public void setCustomFont(TextView textview, String font, Context context) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/sourcesanspro_regular.ttf");
        setTypeface(tf);

    }
}
