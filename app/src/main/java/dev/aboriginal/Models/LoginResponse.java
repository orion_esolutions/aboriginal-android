package dev.aboriginal.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("Message")
    @Expose
    private String message;

    @SerializedName("loginUserDetails")
    @Expose
    private LoginUserDetails loginUserDetails;

    @SerializedName("obj")
    @Expose
    private Obj obj;

    @SerializedName("status")
    @Expose
    private Boolean status;

    public LoginUserDetails getLoginUserDetails() {
        return loginUserDetails;
    }

    public void setLoginUserDetails(LoginUserDetails loginUserDetails) {
        this.loginUserDetails = loginUserDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Obj getObj() {
        return obj;
    }

    public void setObj(Obj obj) {
        this.obj = obj;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }



    public class Obj {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("UserName")
        @Expose
        private String userName;
        @SerializedName("Email")
        @Expose
        private String email;
        @SerializedName("Password")
        @Expose
        private Object password;
        @SerializedName("MobileNumber")
        @Expose
        private String mobileNumber;
        @SerializedName("ResetPasswordOTP")
        @Expose
        private Object resetPasswordOTP;
        @SerializedName("OTPExpiry")
        @Expose
        private Object oTPExpiry;
        @SerializedName("FirstName")
        @Expose
        private Object firstName;
        @SerializedName("LastName")
        @Expose
        private Object lastName;
        @SerializedName("isActive")
        @Expose
        private Object isActive;
        @SerializedName("isDeleted")
        @Expose
        private Object isDeleted;
        @SerializedName("CreatedOn")
        @Expose
        private Object createdOn;
        @SerializedName("ModifiedOn")
        @Expose
        private Object modifiedOn;
        @SerializedName("CreatedBy")
        @Expose
        private Object createdBy;
        @SerializedName("ModifiedBy")
        @Expose
        private Object modifiedBy;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getPassword() {
            return password;
        }

        public void setPassword(Object password) {
            this.password = password;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public Object getResetPasswordOTP() {
            return resetPasswordOTP;
        }

        public void setResetPasswordOTP(Object resetPasswordOTP) {
            this.resetPasswordOTP = resetPasswordOTP;
        }

        public Object getOTPExpiry() {
            return oTPExpiry;
        }

        public void setOTPExpiry(Object oTPExpiry) {
            this.oTPExpiry = oTPExpiry;
        }

        public Object getFirstName() {
            return firstName;
        }

        public void setFirstName(Object firstName) {
            this.firstName = firstName;
        }

        public Object getLastName() {
            return lastName;
        }

        public void setLastName(Object lastName) {
            this.lastName = lastName;
        }

        public Object getIsActive() {
            return isActive;
        }

        public void setIsActive(Object isActive) {
            this.isActive = isActive;
        }

        public Object getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(Object isDeleted) {
            this.isDeleted = isDeleted;
        }

        public Object getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Object createdOn) {
            this.createdOn = createdOn;
        }

        public Object getModifiedOn() {
            return modifiedOn;
        }

        public void setModifiedOn(Object modifiedOn) {
            this.modifiedOn = modifiedOn;
        }

        public Object getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Object createdBy) {
            this.createdBy = createdBy;
        }

        public Object getModifiedBy() {
            return modifiedBy;
        }

        public void setModifiedBy(Object modifiedBy) {
            this.modifiedBy = modifiedBy;
        }
    }

    public class LoginUserDetails {

        @SerializedName("UserDetails")
        @Expose
        private UserDetails userDetails;
     /*   @SerializedName("PatientTeam")
        @Expose
        private List<PatientTeam> patientTeam = new ArrayList<PatientTeam>();*/

        public UserDetails getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(UserDetails userDetails) {
            this.userDetails = userDetails;
        }

      /*  public List<PatientTeam> getPatientTeam() {
            return patientTeam;
        }

        public void setPatientTeam(List<PatientTeam> patientTeam) {
            this.patientTeam = patientTeam;
        }
*/
    }

    public class UserDetails {

        @SerializedName("Id")
        @Expose
        private Integer id;

        @SerializedName("UserName")
        @Expose
        private String userName;

        @SerializedName("Email")
        @Expose
        private String email;

        @SerializedName("MobileNumber")
        @Expose
        private String mobileNumber;

        @SerializedName("FirstName")
        @Expose
        private Object firstName;

        @SerializedName("LastName")
        @Expose
        private Object lastName;

        @SerializedName("isActive")
        @Expose
        private Object isActive;

        @SerializedName("isDeleted")
        @Expose
        private Object isDeleted;

        @SerializedName("isPatient")
        @Expose
        private Boolean isPatient;

        public Boolean getPatient() {
            return isPatient;
        }

        public void setPatient(Boolean patient) {
            isPatient = patient;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public Object getFirstName() {
            return firstName;
        }

        public void setFirstName(Object firstName) {
            this.firstName = firstName;
        }

        public Object getLastName() {
            return lastName;
        }

        public void setLastName(Object lastName) {
            this.lastName = lastName;
        }

        public Object getIsActive() {
            return isActive;
        }

        public void setIsActive(Object isActive) {
            this.isActive = isActive;
        }

        public Object getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(Object isDeleted) {
            this.isDeleted = isDeleted;
        }
}}
