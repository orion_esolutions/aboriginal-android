package dev.aboriginal.Models;

import java.util.Date;

public class MessageModel {


    public String message;
    public String date;
    public String timestamp;
    public int messageType;
    public int messageid;
    public Date messageTime = new Date();

    public MessageModel() {

    }

    // Constructor
    public MessageModel(String message, String date, int messageType) {
        this.message = message;
        this.messageType = messageType;
        this.date = date;
        this.date = date;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Date getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(Date messageTime) {
        this.messageTime = messageTime;
    }

    public int getMessageid() {
        return messageid;
    }

    public void setMessageid(int messageid) {
        this.messageid = messageid;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }
}
