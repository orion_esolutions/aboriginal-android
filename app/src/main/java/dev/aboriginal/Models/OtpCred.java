package dev.aboriginal.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtpCred {

    @SerializedName("email")
    @Expose
    private String email;


    @SerializedName("OTP")
    @Expose
    private String OTP;

    public OtpCred(String email,String OTP) {
        this.email = email;
        this.OTP = OTP;



    }
}
