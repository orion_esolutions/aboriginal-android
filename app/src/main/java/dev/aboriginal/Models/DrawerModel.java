package dev.aboriginal.Models;

import java.util.ArrayList;

/**
 * Created by Android on 11/8/2017.
 */

public class DrawerModel {

    Result1 Result;

    public DrawerModel.Result1 getResult() {
        return Result;
    }

    public void setResult(DrawerModel.Result1 result) {
        Result = Result;
    }

    public class Result1{



    String LastUpdatedDate;
    ArrayList<IstTabs> lstTabs=new ArrayList();



    public String getLastUpdatedDate() {
        return LastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        LastUpdatedDate = lastUpdatedDate;
    }

    public ArrayList<IstTabs> getIstTabs() {
        return lstTabs;
    }

    public void setIstTabs(ArrayList<IstTabs> istTabs) {
        this.lstTabs = istTabs;
    }

    public class  IstTabs{

        String TabName;
        String StaffNotes;
        int Sorting;
        ArrayList<subTabDetails> SubTabDetails=new ArrayList<>();

        public String getTabName() {
            return TabName;
        }

        public void setTabName(String tabName) {
            TabName = tabName;
        }

        public String getStaffNotes() {
            return StaffNotes;
        }

        public void setStaffNotes(String staffNotes) {
            StaffNotes = staffNotes;
        }

        public int getSorting() {
            return Sorting;
        }

        public void setSorting(int sorting) {
            Sorting = sorting;
        }

        public ArrayList<subTabDetails> getSubTabDetails() {
            return SubTabDetails;
        }

        public void setSubTabDetails(ArrayList<subTabDetails> subTabDetails) {
            this.SubTabDetails = subTabDetails;
        }

        public class subTabDetails{

            String SubTabName;
            String Description;

            public String getSubTabName() {
                return SubTabName;
            }

            public void setSubTabName(String subTabName) {
                SubTabName = subTabName;
            }

            public String getDescription() {
                return Description;
            }

            public void setDescription(String description) {
                Description = description;
            }
        }
    }
    }
}
