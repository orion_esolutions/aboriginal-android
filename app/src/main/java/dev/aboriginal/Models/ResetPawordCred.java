package dev.aboriginal.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResetPawordCred {


    @SerializedName("email")
    @Expose
    private String email;


    @SerializedName("OTP")
    @Expose
    private String OTP;

    @SerializedName("NewPassword")
    @Expose
    private String NewPassword;

    public ResetPawordCred(String email,String OTP,String NewPassword) {
        this.email = email;
        this.OTP = OTP;
        this.NewPassword = NewPassword;



    }
}
