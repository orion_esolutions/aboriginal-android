package dev.aboriginal.Models;

import java.util.Date;

public class ChannelModel {


    public String message;
    public String name;
    public String id;
    public boolean isRead;

    // Constructor
    public ChannelModel(String name, String message, String id, boolean isRead) {
        this.message = message;
        this.name = name;
        this.id = id;
        this.isRead = isRead;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
