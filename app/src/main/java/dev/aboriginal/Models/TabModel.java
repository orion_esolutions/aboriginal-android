package dev.aboriginal.Models;

/**
 * Created by Android on 11/9/2017.
 */

public class TabModel {

    String id;
    String TabName;
    String StaffNotes;
    String Sorting;
    String size;
    String description;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTabName() {
        return TabName;
    }

    public void setTabName(String tabName) {
        TabName = tabName;
    }

    public String getStaffNotes() {
        return StaffNotes;
    }

    public void setStaffNotes(String staffNotes) {
        StaffNotes = staffNotes;
    }

    public String getSorting() {
        return Sorting;
    }

    public void setSorting(String sorting) {
        Sorting = sorting;
    }
}
