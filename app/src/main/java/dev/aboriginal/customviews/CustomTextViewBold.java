package dev.aboriginal.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by Android on 11/2/2016.
 */
public class CustomTextViewBold extends androidx.appcompat.widget.AppCompatTextView {
    public CustomTextViewBold(Context context) {
        super(context);
        init();

    }

//    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);        init();
//
//
//    }

    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);        init();


    }

    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Roboto-Medium.ttf");
            setTypeface(tf);
        }
    }
}
