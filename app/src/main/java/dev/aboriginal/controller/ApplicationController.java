package dev.aboriginal.controller;

import dev.aboriginal.R;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.splunk.mint.Mint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import data.BaseManagerInterface;
import data.listeners.BaseUIListener;
import data.listeners.OnInitializedListener;
import data.listeners.OnLoadListener;
import dev.aboriginal.chatmodule.prefs.Prefs;

import static android.os.Build.VERSION.SDK_INT;

/**
 * Created by Android on 11/2/2016.
 */
public class ApplicationController extends Application {

    public static final String TAG = Application.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private final ArrayList<Object> registeredManagers;

    private ImageLoader mImageLoader;

    private static ApplicationController mInstance;


    /**
     * Unmodifiable collections of managers that implement some common
     * interface.
     */
    private Map<Class<? extends BaseManagerInterface>, Collection<? extends BaseManagerInterface>> managerInterfaces;

    private Map<Class<? extends BaseUIListener>, Collection<? extends BaseUIListener>> uiListeners;

    /**
     * Thread to execute tasks in background..
     */
    private final ExecutorService backgroundExecutor;

    /**
     * Handler to execute runnable in UI thread.
     */
    private final Handler handler;

    /**
     * Where data load was requested.
     */
    private boolean serviceStarted;

    /**
     * Whether application was initialized.
     */
    private boolean initialized;

    /**
     * Whether user was notified about some action in contact list activity
     * after application initialization.
     */
    private boolean notified;

    /**
     * Whether application is to be closed.
     */
    private boolean closing;

    /**
     * Whether {@link #onServiceDestroy()} has been called.
     */
    private boolean closed;

    /**
     * Future for loading process.
     */
    private Future<Void> loadFuture;

//	private final Runnable timerRunnable = new Runnable() {
//
//		@Override
//		public void run() {
//			for (OnTimerListener listener : getManagers(OnTimerListener.class))
//				listener.onTimer();
//			if (!closing)
//				startTimer();
//		}
//
//	};

    public ApplicationController() {
        mInstance = this;
        serviceStarted = false;
        initialized = false;
        notified = false;
        closing = false;
        closed = false;
        uiListeners = new HashMap<Class<? extends BaseUIListener>, Collection<? extends BaseUIListener>>();
        managerInterfaces = new HashMap<Class<? extends BaseManagerInterface>, Collection<? extends BaseManagerInterface>>();
        registeredManagers = new ArrayList<Object>();

        handler = new Handler();
        backgroundExecutor = Executors
                .newSingleThreadExecutor(new ThreadFactory() {
                    @Override
                    public Thread newThread(Runnable runnable) {
                        Thread thread = new Thread(runnable,
                                "Background executor service");
                        thread.setPriority(Thread.MIN_PRIORITY);
                        thread.setDaemon(true);
                        return thread;
                    }
                });
    }

    /**
     * Whether application is initialized.
     */
    public boolean isInitialized() {
        return initialized;
    }

    private void onLoad() {
        for (OnLoadListener listener : getManagers(OnLoadListener.class)) {
            //LogManager.i(listener, "onLoad");
            listener.onLoad();
        }
    }
    //
    private void onInitialized() {
        for (OnInitializedListener listener : getManagers(OnInitializedListener.class)) {
            //LogManager.i(listener, "onInitialized");
            listener.onInitialized();
        }
        initialized = true;

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//		MultiDex.install(this);
    }

    private void initGTM(){
//		TagManager tagManager = TagManager.getInstance(this);
//
//		//TODO : input your gtm info
//		PendingResult<ContainerHolder> pending = tagManager.loadContainerPreferNonDefault(Constants.CONTAINER_ID,R.raw.gtm/* your gtm default container resource */);
//
//		pending.setResultCallback(new ResultCallback<ContainerHolder>() {
//			@Override
//			public void onResult(ContainerHolder containerHolder) {
//				if(!containerHolder.getStatus().isSuccess()){
//					Log.e("Sandy", "failure loading container");
//					return;
//				}
//
//				ContainerHolderSingleton.setContainerHolder(containerHolder);
//				containerHolder.setContainerAvailableListener(new ContainerHolder.ContainerAvailableListener() {
//					@Override
//					public void onContainerAvailable(ContainerHolder containerHolder, String containerVer) {
//						Log.e("JuL", "containerVer = "+containerVer);
//					}
//				});
//
//
//			}
//		}, 2, TimeUnit.SECONDS);
    }

    @Override
    public void onLowMemory() {
//		for (OnLowMemoryListener listener : getManagers(OnLowMemoryListener.class))
//			listener.onLowMemory();
        super.onLowMemory();
    }

    /**
     * Service have been destroyed.
     */
    public void onServiceDestroy() {
//		if (closed)
//			return;
//		onClose();
//		runInBackground(new Runnable() {
//			@Override
//			public void run() {
//				onUnload();
//			}
//		}
//		);
    }

    @Override
    public void onTerminate() {
        requestToClose();
        super.onTerminate();
    }

    /**
     * Start periodically callbacks.
     */
    private void startTimer() {
//		runOnUiThreadDelay(timerRunnable, OnTimerListener.DELAY);
    }

    /**
     * Register new manager.
     *
     * @param manager
     */


    /**
     * @param cls
     *            Requested class of managers.
     * @return List of registered manager.
     */
    @SuppressWarnings("unchecked")
    public <T extends BaseManagerInterface> Collection<T> getManagers(Class<T> cls) {
        if (closed)
            return Collections.emptyList();
        Collection<T> collection = (Collection<T>) managerInterfaces.get(cls);
        if (collection == null) {
            collection = new ArrayList<T>();
            for (Object manager : registeredManagers)
                if (cls.isInstance(manager))
                    collection.add((T) manager);
            collection = Collections.unmodifiableCollection(collection);
            managerInterfaces.put(cls, collection);
        }
        return collection;
    }

    /**
     * Request to clear application data.
     */
    public void requestToClear() {
//		runInBackground(new Runnable() {
//			@Override
//			public void run() {
//				clear();
//			}
//		});
    }
//	private void onClose() {
////gManager.i(this, "onClose");
//		for (Object manager : registeredManagers)
//			if (manager instanceof OnCloseListener)
//				((OnCloseListener) manager).onClose();
//		closed = true;
//	}
//
//	private void onUnload() {
//		//LogManager.i(this, "onUnload");
//		for (Object manager : registeredManagers)
//			if (manager instanceof OnUnloadListener)
//				((OnUnloadListener) manager).onUnload();
//		android.os.Process.killProcess(android.os.Process.myPid());
//	}

    /**
     * @return <code>true</code> only once per application life. Subsequent
     *         calls will always returns <code>false</code>.
     */
    public boolean doNotify() {
        if (notified)
            return false;
        notified = true;
        return true;
    }

    /**
     * Starts data loading in background if not started yet.
     *
     * @return
     */
    public void onServiceStarted() {
//		if (serviceStarted)
//			return;
//		serviceStarted = true;
//		//LogManager.i(this, "onStart");
//		loadFuture = backgroundExecutor.submit(new Callable<Void>() {
//			@Override
//			public Void call() throws Exception {
//				try {
////					onLoad();
//				} finally {
//					runOnUiThread(new Runnable() {
//						@Override
//						public void run() {
//							// Throw exceptions in UI thread if any.
//							try {
//								loadFuture.get();
//							} catch (InterruptedException e) {
//								throw new RuntimeException(e);
//							} catch (ExecutionException e) {
//								throw new RuntimeException(e);
//							}
//							onInitialized();
//						}
//					});
//				}
//				return null;
//			}
//		});
    }

    /**
     * Requests to close application in some time in future.
     */
    public void requestToClose() {
        closing = true;

    }

    /**
     * @return Whether application is to be closed.
     */
    public boolean isClosing() {
        return closing;
    }

    /**
     * Returns whether system contact storage is supported.
     *
     * Note:
     *
     * Please remove *_CONTACTS, *_ACCOUNTS, *_SETTINGS permissions,
     * SyncAdapterService and AccountAuthenticatorService together from manifest
     * file.
     *
     * @return
     */
    public boolean isContactsSupported() {
        return SDK_INT >= 5
                && checkCallingOrSelfPermission("android.permission.READ_CONTACTS") == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Prefs.initialize(this);

        try {
            Class.forName("android.os.AsyncTask");

            try{
				Mint.initAndStartSession(getApplicationContext(), " 0ad6dde5");

            }catch (Exception e){
                e.printStackTrace();
            }



        }
        catch(Throwable ignore) {

            Log.e("ignore","ignore");

            // ignored
        }


        TypedArray tableClasses = getResources().obtainTypedArray(R.array.tables);
        for (int index = 0; index < tableClasses.length(); index++)
            try {
                Class.forName(tableClasses.getString(index));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        tableClasses.recycle();
//        Mint.initAndStartSession(getApplicationContext(), "0ad6dde5");
//        mInstance = this;
    }

    public static synchronized ApplicationController getInstance() {

        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }
    public void addManager(Object manager) {
        registeredManagers.add(manager);
    }
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
