package dev.aboriginal.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pubnub.api.PubNub;
import com.pubnub.api.enums.PNPushType;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

import dev.aboriginal.R;
import dev.aboriginal.ui.PubNubChatActivity;

public class MyFireBaseMessangingService extends FirebaseMessagingService {

    AppPreferences preferences;
    private PubNub pubnub;

    @Override
    public void onNewToken(@NonNull @NotNull String token) {
        preferences = new AppPreferences(getApplicationContext());
        String oldToken = preferences.getuser_devicetoken();
        if (token.equals(oldToken)) {
            return;
        }
        preferences.setuser_devicetoken(token);

//        ChannelListActivity.updatePushNotificationsOnChannels(preferences.readChannelList(), token, oldToken);
    }

    @Override
    public void onMessageReceived(@NonNull @NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        preferences = new AppPreferences(getApplicationContext());
        Log.e("TAG", "Message data payload: " + remoteMessage.getData());
        if (!preferences.getuser_id().equals(remoteMessage.getData().get("senderid"))) {
            if (remoteMessage.getData().size() > 0) {
                sendNotification(remoteMessage.getData(), Objects.requireNonNull(remoteMessage.getNotification()).getTitle());
            }
        }
    }

    private void sendNotification(Map<String, String> message, String name) {
        Intent intent = new Intent(this, PubNubChatActivity.class);
        intent.putExtra("FromStart", true);
        intent.putExtra("message", message.get("message"));
        intent.putExtra("channel_id", message.get("channel_id"));
        intent.putExtra("channel_name", message.get("channel_name"));

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(message.get("channel_id"), name, importance);
            channel.setDescription(message.get("message"));
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(name)
                .setContentText(message.get("message"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);

        notificationManager.notify(0, notificationBuilder.build());

        updateChannelListActivity(getApplicationContext(), message.get("channel_id"), message.get("message"), message.get("channel_name"));
    }

    static void updateChannelListActivity(Context context, String channel_id, String message, String channel_name) {
        Intent intent = new Intent("update_admin_channel_list");
        intent.putExtra("channel_id", channel_id);
        intent.putExtra("last_message", message);
        intent.putExtra("channel_name", channel_name);
        context.sendBroadcast(intent);
    }

    private void sendRegistrationToPubNub(String token) {
        // Configure PubNub push notifications.
        PubNubChatActivity.pubnub.addPushNotificationsOnChannels()
                .pushType(PNPushType.FCM)
                .channels(Arrays.asList("HelloPush", "TestPushChannel"))
                .deviceId(token)
                .async((result, status) -> {
                    Log.d("PUBNUB", "-->PNStatus.getStatusCode = " + status.getStatusCode());
                });
    }

    public void updatePushNotificationsOnChannels(String[] channels, String deviceToken, String oldToken) {
        if (oldToken != null) {
            pubnub.removeAllPushNotificationsFromDeviceWithPushToken()
                    .pushType(PNPushType.FCM)
                    .deviceId(oldToken)
                    .async((result, status) -> {
                        // Handle Response
                    });
        }

        pubnub.addPushNotificationsOnChannels()
                .pushType(PNPushType.FCM)
                .deviceId(deviceToken)
                .channels(Arrays.asList(channels))
                .async((result, status) -> {
                    // Handle Response
                });
    }
}
