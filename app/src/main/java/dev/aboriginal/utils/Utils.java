package dev.aboriginal.utils;


import dev.aboriginal.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Android on 11/2/2016.
 */

public class Utils {
    public static Utils instance;
    public static String GROUPCHANNELNAME = "ICPGroup3";

    public static Utils getInstance() {
        if (instance == null) {
            synchronized (Utils.class) {
                if (instance == null) {
                    instance = new Utils();
                }
            }
        }
        return instance;
    }

    public static String GetUTCTimetoLOaclConversion(String val) {
        String gmtTime = null;

        try {
            SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            currentDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date sdf = currentDate.parse(val);
            SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm a");
            gmtTime = sdf2.format(sdf);
            Log.e(gmtTime, "hello");


        } catch (Exception e) {
            e.printStackTrace();
        }
        return gmtTime;
    }

    public static String GetUTCDatetoLOaclConversion(String val) {
        String gmtTime = null;

        try {
            SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            currentDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date sdf = currentDate.parse(val);
            SimpleDateFormat sdf2 = new SimpleDateFormat("MMM dd,yyyy");///jan 01,1970
            gmtTime = sdf2.format(sdf);
            Log.e(gmtTime, "hello");


        } catch (Exception e) {
            e.printStackTrace();
        }
        return gmtTime;
    }

    public static boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }


    Dialog dialog;

    public void showCustomDialog(Context context, String title) {


        if (dialog == null) {
            dialog = new Dialog(context, android.R.style.Theme_NoTitleBar);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_loading_dialog);
            dialog.setCancelable(false);
            TextView loadertext = (TextView) dialog.findViewById(R.id.message);
            loadertext.setText(title);
            dialog.show();
        }
    }

    public void cancelCustomDialog() {
        try {
            if (null != dialog && dialog.isShowing()) {
                dialog.cancel();
                dialog = null;
            }
        } catch (Exception e) {
            dialog = null;
            e.printStackTrace();
        }
    }


    public static Gson getGsonObject() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        Gson gson = gsonBuilder.create();
        return gson;
    }


    public static void setImageView(Context context,
                                    String url,
                                    ImageView imageView
    ) {
        if (null != url) {
            if (!url.equalsIgnoreCase("")) {
                Picasso.with(context)
                        .load(url)
                        .into(imageView);
            } else {
                Picasso.with(context)
                        .load(url)
                        .into(imageView);
            }
        } else {
            Picasso.with(context)
                    .load(url)
                    .into(imageView);
        }
    }

    public static boolean isMarshMallow() {
        if (android.os.Build.VERSION.SDK_INT >= 23)
            return true;
        else
            return false;
    }

    public static Bitmap rotate(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                source.getHeight(), matrix, false);
    }

    public static void showErrorMessage(int message, Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setContentText(context.getResources().getString(message))
                .show();

    }

    public static void showErrorMessage(String message, Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setContentText(message)
                .show();

    }

    public static void showErrorMessagestatus(String message, String buttonname, Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setContentText(message)
                .setConfirmText(buttonname)
                .show();

    }


    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }


    public boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

}
