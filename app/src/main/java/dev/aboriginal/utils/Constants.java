package dev.aboriginal.utils;

/**
 * Created by Android on 11/1/2017.
 */

public class Constants {
    public static final int CONNECTION_TIMEOUT = 100;

    public static final String DATABASE_NAME = "aboriginal.db";
    public static final int DATABASE_VERSION = 8;
    public static final int DATABASE_VERSION_06 = 5;
    public static final int DATABASE_VERSION_07 = 6;
    public static final int DATABASE_VERSION_08 = 8;



    //............................ Web Constants..............

    public static final String DRAWER_ITEMS_API = "drawer_item_api";
    public static final String Api_url = "https://aboriginaldev.customerdemourl.com/api/";
}
