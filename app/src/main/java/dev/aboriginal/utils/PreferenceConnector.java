package dev.aboriginal.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class PreferenceConnector {
	
	
	Context mContext;
	String title;
	boolean isClicked;
	SharedPreferences sharedPreferences;
	Editor editor;

	public static PreferenceConnector instance;

	public static PreferenceConnector getInstance(Context activity) {
		if (instance == null) {

			instance = new PreferenceConnector(activity);

		}
		return instance;
	}


	public PreferenceConnector(Context activity) {
		super();
		mContext = activity;
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		editor = sharedPreferences.edit();
	}
	

	public void savePreferences(String key, String value) {
		editor.putString(key, value);
		editor.commit();
	}
	
	public void savePreferences(String key, int value) {
		editor.putInt(key, value);
		editor.commit();
	}

	public void saveFloatPreferences(String key, float value) {
		editor.putFloat(key, value);
		editor.commit();
	}


	public float loadSavedFloatPreferences(String title, float defaultValue) {
		return sharedPreferences.getFloat(title,defaultValue);

	}
	
		
	public void savePreferences(String key, boolean value) {
		editor.putBoolean(key, value);
		editor.commit();
	}

	public String loadSavedPreferences(String title, String defaultText) {

		return sharedPreferences.getString(title, defaultText);

	}
	
	public int loadSavedPreferences(String title, int defaultText) {
		return sharedPreferences.getInt(title, defaultText);

	}
	
	public boolean loadBooleanPreferences(String title) {

		return sharedPreferences.getBoolean(title, false);

	}
}
