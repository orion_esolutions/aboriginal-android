package dev.aboriginal.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import androidx.annotation.Nullable;

import java.util.Set;


public class AppPreferences {

    public SharedPreferences appSharedPrefs;
    public static final String USER_PREFS = "USER_PREFS";
    public Editor prefsEditor;

    Context context;

    public String user_id = "user_id";
    public String user_name = "user_name";
    public String user_first_name = "user_first_name";
    public String user_last_name = "user_last_name";
    public String is_first_time = "isFirstTime";
    public String user_devicetoken = "user_devicetoken";
    public String user_is_admin = "user_is_admin";


    public AppPreferences(Context context) {
        this.context = context;
        this.appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    public boolean getuser_is_admin(){
        return appSharedPrefs.getBoolean(user_is_admin, false);
    }

    public void setuser_is_admin(boolean _user_is_admin){
        this.prefsEditor.putBoolean(user_is_admin, _user_is_admin).commit();
    }

    public boolean getIsFirstTime() {
        return appSharedPrefs.getBoolean(is_first_time, true);
    }

    public void setIsFirstTime(boolean isFirstTime) {
        this.prefsEditor.putBoolean(is_first_time, isFirstTime).apply();
    }

    public String getuser_last_name() {
        return appSharedPrefs.getString(user_last_name, "");
    }

    public void setuser_last_name(String _user_last_name) {
        this.prefsEditor.putString(user_last_name, _user_last_name).commit();
    }

    public String getuser_first_name() {
        return appSharedPrefs.getString(user_first_name, "");
    }

    public void setuser_first_name(String _user_first_name) {
        this.prefsEditor.putString(user_first_name, _user_first_name).commit();
    }

    public String getuser_name() {
        return appSharedPrefs.getString(user_name, "");
    }

    public void setuser_name(String _user_name) {
        this.prefsEditor.putString(user_name, _user_name).commit();
    }

    public String getuser_id() {
        return appSharedPrefs.getString(user_id, "");
    }

    public void setuser_id(String _user_id) {
        this.prefsEditor.putString(user_id, _user_id).commit();
    }

    public String getuser_devicetoken() {
        return appSharedPrefs.getString(user_devicetoken, "");
    }

    public void setuser_devicetoken(String _user_id) {
        this.prefsEditor.putString(user_devicetoken, _user_id).commit();
    }

    public void clearSharePreferances(Context context) {
        this.prefsEditor.clear().commit();
    }

    public String FCM_CHANNEL_LIST = "PUBNUB_FCM_CHANNEL_LIST";

    public @Nullable
    String[] readChannelList() {
        return (String[]) appSharedPrefs.getStringSet(FCM_CHANNEL_LIST, null).toArray();
    }

    public void writeChannelList(Set<String> value) {
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        prefsEditor.putStringSet(FCM_CHANNEL_LIST, value);
        prefsEditor.apply();
    }
}
