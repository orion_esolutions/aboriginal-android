package dev.aboriginal.chatmodule.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import dev.aboriginal.CustomAdapter;
import dev.aboriginal.Models.ChannelModel;
import dev.aboriginal.R;
import dev.aboriginal.ui.PubNubChatActivity;

public class ChannelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    ArrayList<ChannelModel> list;

    public ChannelAdapter(Context context, ArrayList<ChannelModel> list) { // you can pass other parameters in constructor
        this.context = context;
        this.list = list;
    }

    private class MessageInViewHolder extends RecyclerView.ViewHolder {

        TextView tvMessage, tvName, tvLabel;
        ImageView dot;
        ConstraintLayout main;

        MessageInViewHolder(final View itemView) {
            super(itemView);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvName = itemView.findViewById(R.id.tvName);
            dot = itemView.findViewById(R.id.dot);
            main = itemView.findViewById(R.id.main);
            tvLabel = itemView.findViewById(R.id.tvLabel);
        }

        void bind(int position) {
            ChannelModel messageModel = list.get(position);
            tvName.setText(messageModel.name);
            tvMessage.setText(messageModel.message);

            if (!messageModel.isRead) {
                dot.setVisibility(View.VISIBLE);
//                tvLabel.setVisibility(View.VISIBLE);
                tvMessage.setTypeface(null, Typeface.BOLD_ITALIC);

            } else {
                dot.setVisibility(View.GONE);
//                tvLabel.setVisibility(View.GONE);
                tvMessage.setTypeface(null, Typeface.NORMAL);
            }

            main.setOnClickListener(view -> {
                Intent i = new Intent(context, PubNubChatActivity.class);
                i.putExtra("channel_id", messageModel.id);
                i.putExtra("channel_name", messageModel.name);
                i.putExtra("message", messageModel.message);
                context.startActivity(i);
            });
        }
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new MessageInViewHolder(LayoutInflater.from(context).inflate(R.layout.channel_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        ((MessageInViewHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
