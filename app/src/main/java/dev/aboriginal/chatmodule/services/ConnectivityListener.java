package dev.aboriginal.chatmodule.services;

public interface ConnectivityListener {

    void onConnected();

}
