package dev.aboriginal.WebServices;

import dev.aboriginal.utils.Constants;
import dev.aboriginal.utils.Utils;
import android.app.Activity;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.Map;


/**
 * Created by Android on 11/7/2016.
 */
public class PostMethod {

    JsonObjectRequest jsonObjectRequest = null;
    final String TAG = "POST METHOD";
    private boolean isResponseOk = false;


    public PostMethod(final String url, final Map<String, String> hashmap, final Activity activity, final String tag, String msg) {
        Log.e("hashmap", "" + hashmap.toString());
         Utils.getInstance().showCustomDialog(activity, msg);


        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(hashmap),
                    new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {


                            Log.e(TAG, response.toString());
                            parseData(tag, response,activity);
                            Utils.getInstance().cancelCustomDialog();

                    }


                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Utils.getInstance().cancelCustomDialog();
                Utils.showErrorMessage("Unable to connect. Please check that you are connected to the Internet and try again.", activity);
                // hide the progress dialog
                Utils.getInstance().cancelCustomDialog();
            }

        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }



    public PostMethod(final String url, final Activity activity, final String tag, String msg) {

        Utils.getInstance().showCustomDialog(activity, msg);


        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {


                            Log.e(TAG, response.toString());
                            parseData(tag, response,activity);
                            Utils.getInstance().cancelCustomDialog();

                    }


                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Utils.getInstance().cancelCustomDialog();
                Utils.showErrorMessage("Unable to connect. Please check that you are connected to the Internet and try again.", activity);
                // hide the progress dialog
                Utils.getInstance().cancelCustomDialog();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

//


    public PostMethod(final String url, JSONObject hashmap, final Activity activity, final String tag, String msg) {
        Log.e("hashmap", "" + hashmap.toString());
        if(msg.equalsIgnoreCase("Searching...")){

        }
        else {
            Utils.getInstance().showCustomDialog(activity, msg);

        }


        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url,hashmap,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {


                            Log.e(TAG, response.toString());
                            parseData(tag, response,activity);
                            Utils.getInstance().cancelCustomDialog();

                    }


                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Utils.getInstance().cancelCustomDialog();
                Utils.showErrorMessage("Unable to connect. Please check that you are connected to the Internet and try again.", activity);

                // hide the progress dialog

            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

//
    public JsonObjectRequest getStringRequest() {
        return jsonObjectRequest;
    }

    public void parseData(String tag, JSONObject response, Activity activity) {

        switch (tag) {

//            case Constants.LOGIN_API:
////              =new LoginModel();
//
//                LoginModel loginModel = Utils.getGsonObject().fromJson(response.toString(), LoginModel.class);
//                 //   onResultListener.onResult(loginModel);
//                 EventBus.getDefault().post(loginModel);
//
//                break;

    }
}}
