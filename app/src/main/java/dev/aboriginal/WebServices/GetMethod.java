package dev.aboriginal.WebServices;

import dev.aboriginal.Models.DrawerModel;
import dev.aboriginal.OnResult;
import dev.aboriginal.utils.Constants;
import dev.aboriginal.utils.Utils;
import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import de.greenrobot.event.EventBus;


/**
 * Created by Android on 11/7/2016.
 */
public class GetMethod {
    JsonObjectRequest jsonObjectRequest;
    final String TAG="GET METHOD";
    OnResult onResult;
    public GetMethod(String url, final Activity activity, final String tag, final String msg, OnResult onResult) {
        Log.e("Response","response");
        this.onResult=onResult;
        if(!msg.equalsIgnoreCase("Refresh"))
        Utils.getInstance().showCustomDialog(activity, msg);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, response.toString());
                        try {
                            parseData(tag, response);

                        } catch (Exception e) {
                            e.printStackTrace();
//                            Utils.showErrorMessage(context.getString(R.string.error_parsing_string), context);
                        }
                        if(!msg.equalsIgnoreCase("Refresh"))
                            Utils.getInstance().cancelCustomDialog();
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                Utils.getInstance().cancelCustomDialog();
                Toast.makeText(activity, "Unable to connect. Please check that you are connected to the Internet and try again.", Toast.LENGTH_SHORT).show();
//                Utils.showErrorMessage("Unable to connect. Please check that you are connected to the Internet and try again.", activity);
            }
        });

    }
    public JsonObjectRequest getStringRequest() {
        return jsonObjectRequest;
    }
    public void parseData(String tag, JSONObject response){

        switch (tag){

            case Constants.DRAWER_ITEMS_API:
                DrawerModel newsFeedModel= Utils.getGsonObject().fromJson(response.toString(),DrawerModel.class);
                Log.e("rsponseData",newsFeedModel.getResult().toString());
                 onResult.onResult(newsFeedModel);
                 onResult.onResult(0);
                EventBus.getDefault().post(newsFeedModel);
                break;

        }

    }
}
