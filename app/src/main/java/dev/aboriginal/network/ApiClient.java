package dev.aboriginal.network;

import dev.aboriginal.utils.Constants;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static ApiInterface apiService;

    public static ApiInterface getApi() {
        if (apiService == null) {


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.Api_url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            apiService = retrofit.create(ApiInterface.class);
        }
        return apiService;
    }

}
