package dev.aboriginal.network;

import dev.aboriginal.Models.DrawerModel;


import dev.aboriginal.Models.ForgotemailCred;
import dev.aboriginal.Models.ForgotemailResponse;
import dev.aboriginal.Models.LoginCred;
import dev.aboriginal.Models.LoginResponse;
import dev.aboriginal.Models.OtpCred;
import dev.aboriginal.Models.OtpResponse;
import dev.aboriginal.Models.ResetPasswordResponse;
import dev.aboriginal.Models.ResetPawordCred;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface ApiInterface {

    @GET("api/tab/get?date=1970-11-13")
    Call <DrawerModel> getAllService();

    @Headers("Content-Type: application/json")
    @POST("Login/Login")
    Call<LoginResponse> Login(@Body LoginCred loginCred);


    @POST("User/ResetPasswordOTP")
    Call<ForgotemailResponse> forgotEmail(@Body ForgotemailCred forgotemailCred);


    @POST("User/CheckOTP")
    Call<OtpResponse> otpCheck(@Body OtpCred otpCred);

    @POST("User/ResetPassword")
    Call<ResetPasswordResponse> ResetPassword(@Body ResetPawordCred resetPawordCred);


}
