package dev.aboriginal.model;

public class SubTabDetails {

    private String Description;

    private String SubTabName;

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getSubTabName ()
    {
        return SubTabName;
    }

    public void setSubTabName (String SubTabName)
    {
        this.SubTabName = SubTabName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Description = "+Description+", SubTabName = "+SubTabName+"]";
    }
}
