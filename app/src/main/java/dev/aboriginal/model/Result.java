package dev.aboriginal.model;

public class Result {

    private String LastUpdatedDate;

    private LstTabs[] lstTabs;

    public String getLastUpdatedDate ()
    {
        return LastUpdatedDate;
    }

    public void setLastUpdatedDate (String LastUpdatedDate)
    {
        this.LastUpdatedDate = LastUpdatedDate;
    }

    public LstTabs[] getLstTabs ()
    {
        return lstTabs;
    }

    public void setLstTabs (LstTabs[] lstTabs)
    {
        this.lstTabs = lstTabs;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LastUpdatedDate = "+LastUpdatedDate+", lstTabs = "+lstTabs+"]";
    }
}
