package dev.aboriginal.model;

public class DrawerResponse {
    private Result Result;

    public Result getResult ()
    {
        return Result;
    }

    public void setResult (Result Result)
    {
        this.Result = Result;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Result = "+Result+"]";
    }
}
