package dev.aboriginal.model;

public class LstTabs {

    private SubTabDetails[] SubTabDetails;

    private String TabName;

    private String StaffNotes;

    private String Sorting;

    public SubTabDetails[] getSubTabDetails ()
    {
        return SubTabDetails;
    }

    public void setSubTabDetails (SubTabDetails[] SubTabDetails)
    {
        this.SubTabDetails = SubTabDetails;
    }

    public String getTabName ()
    {
        return TabName;
    }

    public void setTabName (String TabName)
    {
        this.TabName = TabName;
    }

    public String getStaffNotes ()
    {
        return StaffNotes;
    }

    public void setStaffNotes (String StaffNotes)
    {
        this.StaffNotes = StaffNotes;
    }

    public String getSorting ()
    {
        return Sorting;
    }

    public void setSorting (String Sorting)
    {
        this.Sorting = Sorting;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SubTabDetails = "+SubTabDetails+", TabName = "+TabName+", StaffNotes = "+StaffNotes+", Sorting = "+Sorting+"]";
    }
}
