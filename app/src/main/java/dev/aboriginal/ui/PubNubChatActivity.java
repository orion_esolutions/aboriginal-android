package dev.aboriginal.ui;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNOperationType;
import com.pubnub.api.enums.PNPushType;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.objects_api.channel.PNChannelMetadataResult;
import com.pubnub.api.models.consumer.objects_api.membership.PNMembershipResult;
import com.pubnub.api.models.consumer.objects_api.uuid.PNUUIDMetadataResult;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.pubnub.api.models.consumer.pubsub.PNSignalResult;
import com.pubnub.api.models.consumer.pubsub.files.PNFileEventResult;
import com.pubnub.api.models.consumer.pubsub.message_actions.PNMessageActionResult;
import com.pubnub.api.models.consumer.push.payload.PushPayloadHelper;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import adapter.PubNubChatAdapter;
import dev.aboriginal.CustomAdapter;
import dev.aboriginal.Models.MessageModel;
import dev.aboriginal.R;
import dev.aboriginal.utils.AppPreferences;
import dev.aboriginal.utils.PaginationListener;
import dev.aboriginal.utils.Utils;

import static dev.aboriginal.utils.PaginationListener.PAGE_START;

public class PubNubChatActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final Integer CHUNK_SIZE = 100;
    TextView tvBack;
    public static PubNub pubnub;
    String channel_id = "", channel_name = "", user_first_name = "", user_last_name = "";
    AppPreferences appPreferences;
    String TAG = PubNubChatActivity.class.getSimpleName();
    private EditText entryUpdateText;
    PubNubChatAdapter adapter;
    RecyclerView recyclerView;
    private ArrayList<MessageModel> messagesList;
    private ProgressDialog dialog;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private final int totalPage = 5;
    private boolean isLoading = false;

//    Create channel id with name+userid
//Subscribe channel and add this channel into a group channel
//Add Metadata to channel (channel id and channel name)
//Send and get messages in this channel

//    first_name + user_id = channel_name

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pubnub_chat_activity);

        Objects.requireNonNull(getSupportActionBar()).hide();
        appPreferences = new AppPreferences(this);

        if (getIntent().getExtras() != null) {
            channel_id = getIntent().getStringExtra("channel_id");
            channel_name = getIntent().getStringExtra("channel_name");
            String message = getIntent().getStringExtra("message");

            pubNubListenerInitialize();
            Log.e(TAG, "channel_id : " + channel_id);
            AddMetadataToChannel(message, true);
        } else {
            channel_id = appPreferences.getuser_name() + appPreferences.getuser_id();
            channel_name = appPreferences.getuser_first_name() + " " + appPreferences.getuser_last_name();
            user_first_name = appPreferences.getuser_first_name();
            user_last_name = appPreferences.getuser_last_name();

            Log.e(TAG, "channel_id : " + channel_id + " , channel_name : " + channel_name + " , user_first_name : " + user_first_name + " , user_last_name : " + user_last_name);

            pubNubListenerInitialize();
            addNotificationChannel(Collections.singletonList(channel_id));
            if (appPreferences.getIsFirstTime()) {
                AddMetadataToChannel("", true);
                appPreferences.setIsFirstTime(false);
            }
        }

        setData();
    }

    @Override
    protected void onDestroy() {
        pubnub.destroy();
        super.onDestroy();
        dismissDialog();
    }

    long lastTime;

    private void fetchHistory() {
        ArrayList<MessageModel> items = new ArrayList<>();
        pubnub.history()
                .channel(channel_id) // where to fetch history from
                .count(CHUNK_SIZE) // how many items to fetch
//                .start(start)
//                .end(end)
                .includeTimetoken(true)
                .async((result, status) -> new Thread(() -> {

                    dismissDialog();
                    if (!status.isError()) {
                        Log.e(TAG, ">> result 11 >>. " + result);
                        if (result != null) {
                            if (!status.isError() && !result.getMessages().isEmpty()) {
                                for (int i = 0; i < result.getMessages().size(); i++) {
                                    String msj = "";
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("message") != null) {
                                        msj = result.getMessages().get(i).getEntry().getAsJsonObject().get("message").getAsString();
                                    }
                                    String id = channel_id;
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("senderid") != null) {
                                        id = result.getMessages().get(i).getEntry().getAsJsonObject().get("senderid").getAsString();
                                    }
                                    String date = "";
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("date") != null) {
                                        date = result.getMessages().get(i).getEntry().getAsJsonObject().get("date").getAsString();
                                    }

                                    lastTime = result.getMessages().get(0).getTimetoken();

                                    String finalDate = date;
                                    String finalMsj = msj;
                                    String finalId = id;
                                    runOnUiThread(() -> {
                                        MessageModel postItem = new MessageModel();
                                        postItem.setMessage(finalMsj);

                                        if (appPreferences.getuser_id().equalsIgnoreCase(finalId)) {
                                            postItem.setDate(finalDate);
                                            postItem.setMessageType(CustomAdapter.MESSAGE_TYPE_OUT);
                                            items.add(postItem);
                                        } else {
                                            postItem.setDate(finalDate);
                                            postItem.setMessageType(CustomAdapter.MESSAGE_TYPE_IN);
                                            items.add(postItem);
                                        }
                                    });
                                }

                                runOnUiThread(() -> {
                                    recyclerView.scrollToPosition(0);

                                    if (items.size() > 0) {
                                        if (currentPage != PAGE_START) adapter.removeLoading();
                                        Collections.reverse(items);
                                        adapter.addItems(items);
                                        swipeRefresh.setRefreshing(false);
                                        // check weather is last page or not
                                        Log.e("TAG", ">> Page >> " + currentPage + " >> " + totalPage);

                                        if (items.size() >= 100) {
                                            if (currentPage < totalPage) {
                                                adapter.addLoading();
                                            } else {
                                                isLastPage = true;
                                            }
                                        } else {
                                            isLastPage = true;
                                        }
                                    } else {
                                        adapter.removeLoading();
                                        isLastPage = true;
                                    }
                                    isLoading = false;
                                });
                            }
                        }

                    } else {
                        status.getErrorData().getThrowable().printStackTrace();
                    }
                }).start());
    }

    private void fetchNextHistory() {
        Log.e(TAG, ">> lastTime 11 >>. " + lastTime);
        ArrayList<MessageModel> items = new ArrayList<>();
        pubnub.history()
                .channel(channel_id) // where to fetch history from
                .count(CHUNK_SIZE) // how many items to fetch
                .start(lastTime)
                .includeTimetoken(true)
                .async((result, status) -> new Thread(() -> {
                    dismissDialog();
                    if (!status.isError()) {
                        Log.e(TAG, ">> result 11 >>. " + result);
                        if (result != null) {
                            if (!status.isError() && !result.getMessages().isEmpty()) {
                                for (int i = 0; i < result.getMessages().size(); i++) {
                                    String msj = "";
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("message") != null) {
                                        msj = result.getMessages().get(i).getEntry().getAsJsonObject().get("message").getAsString();
                                    }
                                    String id = channel_id;
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("senderid") != null) {
                                        id = result.getMessages().get(i).getEntry().getAsJsonObject().get("senderid").getAsString();
                                    }
                                    String date = "";
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("date") != null) {
                                        date = result.getMessages().get(i).getEntry().getAsJsonObject().get("date").getAsString();
                                    }

                                    lastTime = result.getMessages().get(0).getTimetoken();

                                    String finalDate = date;
                                    String finalMsj = msj;
                                    String finalId = id;
                                    runOnUiThread(() -> {
                                        MessageModel postItem = new MessageModel();
                                        postItem.setMessage(finalMsj);

                                        if (appPreferences.getuser_id().equalsIgnoreCase(finalId)) {
                                            postItem.setDate(finalDate);
                                            postItem.setMessageType(CustomAdapter.MESSAGE_TYPE_OUT);
                                            items.add(postItem);
                                        } else {
                                            postItem.setDate(finalDate);
                                            postItem.setMessageType(CustomAdapter.MESSAGE_TYPE_IN);
                                            items.add(postItem);
                                        }
                                    });
                                }

                                runOnUiThread(() -> {
//                                    recyclerView.scrollToPosition(0);

                                    if (items.size() > 0) {
                                        if (currentPage != PAGE_START) adapter.removeLoading();
                                        Collections.reverse(items);
                                        adapter.addItems(items);
                                        swipeRefresh.setRefreshing(false);
                                        // check weather is last page or not
                                        Log.e("TAG", ">> Page >> " + currentPage + " >> " + totalPage);

                                        if (items.size() >= 100) {
                                            if (currentPage < totalPage) {
                                                adapter.addLoading();
                                            } else {
                                                isLastPage = true;
                                            }
                                        } else {
                                            isLastPage = true;
                                        }
                                    } else {
                                        adapter.removeLoading();
                                        isLastPage = true;
                                    }
                                    isLoading = false;
                                });
                            } else {
                                adapter.removeLoading();
                                isLastPage = true;
                                isLoading = false;
                            }
                        }

                    } else {
                        status.getErrorData().getThrowable().printStackTrace();
                    }
                }).start());
    }

    private void fetchHistory1() {
        pubnub.history()
                .channel(channel_id) // where to fetch history from
                .count(CHUNK_SIZE) // how many items to fetch
                .includeTimetoken(true)
                .async((result, status) -> new Thread(() -> {
                    Log.e(TAG, ">> result 11 >>. " + result);
                    dismissDialog();
                    if (!status.isError()) {
                        if (result != null) {
                            if (!status.isError() && !result.getMessages().isEmpty()) {

                                for (int i = 0; i < result.getMessages().size(); i++) {
                                    String msj = "";
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("message") != null) {
                                        msj = result.getMessages().get(i).getEntry().getAsJsonObject().get("message").getAsString();
                                    }
                                    String id = channel_id;
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("senderid") != null) {
                                        id = result.getMessages().get(i).getEntry().getAsJsonObject().get("senderid").getAsString();
                                    }
                                    String date = "";
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("date") != null) {
                                        date = result.getMessages().get(i).getEntry().getAsJsonObject().get("date").getAsString();
                                    }

                                    String finalDate = date;
                                    String finalMsj = msj;
                                    String finalId = id;
                                    runOnUiThread(() -> {
                                        if (appPreferences.getuser_id().equalsIgnoreCase(finalId)) {
                                            messagesList.add(new MessageModel(finalMsj, finalDate, CustomAdapter.MESSAGE_TYPE_OUT));
                                        } else {
                                            messagesList.add(new MessageModel(finalMsj, finalDate, CustomAdapter.MESSAGE_TYPE_IN));
                                        }
                                        adapter.notifyDataSetChanged();
                                        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
                                    });
                                }
                            }
                        }

                    } else {
                        status.getErrorData().getThrowable().printStackTrace();
                    }
                }).start());
    }

    private void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

    private void showDialog() {
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setTitle("Please Wait!");
        dialog.setMessage("Message Loading....");
        dialog.show();
    }

    private void pubNubListenerInitialize() {

        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setPublishKey("pub-c-383f4e4e-2140-4975-a491-b7aa02496a60");
        pnConfiguration.setSubscribeKey("sub-c-87d12ee0-9ea2-11eb-9adf-f2e9c1644994");
        String clientUUID = "lucky_1212";
        pnConfiguration.setUuid(clientUUID);
        pubnub = new PubNub(pnConfiguration);

        createChannel();
        pubnub.subscribe()
                .channels(Collections.singletonList(channel_id))
                .withPresence()
                .execute();

        pubnub.addChannelsToChannelGroup()
                .channelGroup(Utils.GROUPCHANNELNAME)
                .channels(Collections.singletonList(channel_id))
                .async((result, status) -> {
                    if (status.isError()) {
                        Log.e(TAG, ">> Error add Channel to group >> " + status.getErrorData());
                    } else Log.e(TAG, ">> Success add Channel to group >> " + result);
                });

        pubnub.subscribe()
                .channelGroups(Collections.singletonList(Utils.GROUPCHANNELNAME))
                .withPresence()
                .execute();

        showDialog();
        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(@NotNull PubNub pubnub, @NotNull PNStatus status) {
                Log.e(TAG, ">> Response pnStatus >> " + status.getOperation());
//                fetchHistory();
                if (status.getOperation() == PNOperationType.PNSubscribeOperation &&
                        Objects.requireNonNull(status.getAffectedChannelGroups()).contains(Utils.GROUPCHANNELNAME) &&
                        Objects.requireNonNull(status.getAffectedChannels()).contains(channel_id)) {

                    fetchHistory();
                }
            }

            @Override
            public void message(@NotNull PubNub pubnub, @NotNull PNMessageResult message) {

                ArrayList<MessageModel> items = new ArrayList<>();
                String channel = message.getChannel(); // the channel for which the message belongs
//                String channelGroup = message.getSubscription(); // the channel group or wildcard subscription match (if exists)
                Long publishTimetoken = message.getTimetoken(); // publish timetoken
                JsonElement messagePayload = message.getMessage(); // the message payload
                String publisher = message.getPublisher(); // the publisher

                //show message time
                long timetoken = publishTimetoken / 10_000L;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(timetoken);
//                String localDateTime = sdf.format(calendar.getTimeInMillis());

                String msj = "";
                if (messagePayload.getAsJsonObject().get("message") != null) {
                    msj = messagePayload.getAsJsonObject().get("message").getAsString();
                }
                String date = "";
                if (messagePayload.getAsJsonObject().get("date") != null) {
                    date = messagePayload.getAsJsonObject().get("date").getAsString();
                }
                String id = "";
                if (messagePayload.getAsJsonObject().get("senderid") != null) {
                    id = messagePayload.getAsJsonObject().get("senderid").getAsString();
                }
                AddMetadataToChannel(msj, false);
                Log.e(TAG, ">> Channel Name >> " + channel + " , Messages >> " + messagePayload + ", Publisher >> " + publisher);
                String finalId = id;
                String finalMsj = msj;
                String finalDate = date;
                if (!msj.isEmpty() && !id.isEmpty()) {
                    runOnUiThread(() -> {
                        MessageModel postItem = new MessageModel();
                        postItem.setMessage(finalMsj);

                        if (appPreferences.getuser_id().equalsIgnoreCase(finalId)) {
                            postItem.setDate(finalDate);
                            postItem.setMessageType(CustomAdapter.MESSAGE_TYPE_OUT);
                            items.add(postItem);
                        } else {
                            postItem.setDate(finalDate);
                            postItem.setMessageType(CustomAdapter.MESSAGE_TYPE_IN);
                            items.add(postItem);
                        }
//                        adapter.addItems(items);
//                        swipeRefresh.setRefreshing(false);
//                        adapter.removeLoading();

                        if (currentPage != PAGE_START) adapter.removeLoading();
                        adapter.addMessageItems(items);
                        swipeRefresh.setRefreshing(false);
                        // check weather is last page or not
                        Log.e("TAG", ">> Page >> " + currentPage + " >> " + totalPage);
                        if (items.size() >= 100) {
                            if (currentPage < totalPage) {
                                adapter.addLoading();
                            } else {
                                isLastPage = true;
                            }
                        } else {
                            isLastPage = true;
                        }

                        isLoading = false;
                    });
                }
            }

            @Override
            public void presence(@NotNull PubNub pubnub, @NotNull PNPresenceEventResult event) {
//                displayMessage("[PRESENCE: " + event.getEvent() + ']',
//                        "uuid: " + event.getUuid() + ", channel: " + event.getChannel(), "");
            }

            @Override
            public void signal(@NotNull PubNub pubnub, @NotNull PNSignalResult pnSignalResult) {
                Log.e(TAG, ">> Response pnSignalResult >> " + pnSignalResult);
            }

            @Override
            public void uuid(@NotNull PubNub pubnub, @NotNull PNUUIDMetadataResult pnUUIDMetadataResult) {
                Log.e(TAG, ">> Response pnUUIDMetadataResult >> " + pnUUIDMetadataResult);
            }

            @Override
            public void channel(@NotNull PubNub pubnub, @NotNull PNChannelMetadataResult pnChannelMetadataResult) {
                Log.e(TAG, ">> Response pnChannelMetadataResult >> " + pnChannelMetadataResult);
            }

            @Override
            public void membership(@NotNull PubNub pubnub, @NotNull PNMembershipResult pnMembershipResult) {
                Log.e(TAG, ">> Response pnMembershipResult >> " + pnMembershipResult);
            }

            @Override
            public void messageAction(@NotNull PubNub pubnub, @NotNull PNMessageActionResult pnMessageActionResult) {
                Log.e(TAG, ">> Response pnMessageActionResult >> " + pnMessageActionResult);
            }

            @Override
            public void file(@NotNull PubNub pubnub, @NotNull PNFileEventResult pnFileEventResult) {
                Log.e(TAG, ">> Response pnFileEventResult >> " + pnFileEventResult);
            }
        });
    }

    SwipeRefreshLayout swipeRefresh;

    private void setData() {
        messagesList = new ArrayList<>();
        entryUpdateText = findViewById(R.id.entry_update_text);
        swipeRefresh = findViewById(R.id.swipeRefresh);
        recyclerView = findViewById(R.id.recycler_view);
        tvBack = findViewById(R.id.tvBack);

        swipeRefresh.setOnRefreshListener(this);

        adapter = new PubNubChatAdapter(this, messagesList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                fetchNextHistory();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        tvBack.setOnClickListener(v -> onBackPressed());
    }

    private void AddMetadataToChannel(String message, boolean isFirst) {
        Map<String, Object> custom = new HashMap<>();
        if (appPreferences.getuser_id().equalsIgnoreCase("20")) {
            custom.put("message", message);
            custom.put("senderID", appPreferences.getuser_id());
            custom.put("isRead", true);
            custom.put("timestamp", System.currentTimeMillis() / 1000);
        } else {
            if (!isFirst) {
                custom.put("message", message);
                custom.put("senderID", appPreferences.getuser_id());
                custom.put("isRead", false);
                custom.put("timestamp", System.currentTimeMillis() / 1000);
            }
        }

        pubnub.setChannelMetadata()
                .channel(channel_id)
                .name(channel_name)
                .custom(custom)
                .includeCustom(true)
                .async((result, status) -> {
                    if (status.isError()) {
                        //handle error
                        Log.e(TAG, ">> Error add Channel >> " + status.getErrorData());
                    } else {
                        //handle result
                        if (result != null) {
                            Log.e(TAG, ">> Success add Channel >> " + result.getData());
                        }
                    }
                });
    }

    private void notificationSend(String channel, String message, String titleName, String date) {
        PushPayloadHelper pushPayloadHelper = new PushPayloadHelper();
        PushPayloadHelper.FCMPayload fcmPayload = new PushPayloadHelper.FCMPayload();

        PushPayloadHelper.FCMPayload.Notification fcmNotification =
                new PushPayloadHelper.FCMPayload.Notification()
                        .setTitle(titleName)
                        .setBody(message);

        fcmPayload.setNotification(fcmNotification);
        pushPayloadHelper.setFcmPayload(fcmPayload);

        Map<String, Object> commonPayload = new HashMap<>();
        commonPayload.put("message", message);
        commonPayload.put("channel_id", channel);
        commonPayload.put("channel_name", channel);
        commonPayload.put("date", date);
        commonPayload.put("username", appPreferences.getuser_name());
        commonPayload.put("senderid", appPreferences.getuser_id());

        pushPayloadHelper.setCommonPayload(commonPayload);

        Map<String, Object> pushPayload = pushPayloadHelper.build();

        pubnub.publish()
                .channel(channel)
                .message(pushPayload)
                .async((result, status) -> {
                    // Handle Response
                    if (!status.isError()) {
                        Log.e(TAG, ">> Noti Result >> " + result);
                    }
                });
    }

    private void createChannel() {
        // Notification channel should only be created for devices running Android API level 26+.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel chan1 = new NotificationChannel(
                    "default",
                    "default",
                    NotificationManager.IMPORTANCE_HIGH);
            chan1.setLightColor(Color.TRANSPARENT);
            chan1.setLockscreenVisibility(Notification.VISIBILITY_SECRET);
            notificationManager.createNotificationChannel(chan1);
        }
    }

    public void onClick(View view) {
        if (!entryUpdateText.getText().toString().isEmpty()) {
            String date = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date());
            submitUpdate(entryUpdateText.getText().toString(), date);
            entryUpdateText.setText("");
        } else {
            Toast toast;
            toast = Toast.makeText(getApplicationContext(), "Please write a message..!!", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    protected void submitUpdate(String anUpdate, String date) {
        String id = UUID.randomUUID().toString();
        Log.e(TAG, ">> Submit id >> " + id);
        JsonObject entryUpdate = new JsonObject();
        entryUpdate.addProperty("username", appPreferences.getuser_name());
        entryUpdate.addProperty("message", anUpdate);
        entryUpdate.addProperty("messageid", id);
        entryUpdate.addProperty("senderid", appPreferences.getuser_id());
//        entryUpdate.addProperty("date", date);

        Map<String, Object> commonPayload = new HashMap<>();
        commonPayload.put("message", anUpdate);
        commonPayload.put("channel_id", channel_id);
        commonPayload.put("channel_name", channel_name);
        commonPayload.put("messageid", id);
        commonPayload.put("username", appPreferences.getuser_name());
        commonPayload.put("senderid", appPreferences.getuser_id());

        PushPayloadHelper pushPayloadHelper = new PushPayloadHelper();
        PushPayloadHelper.FCMPayload fcmPayload = new PushPayloadHelper.FCMPayload();
        PushPayloadHelper.FCMPayload.Notification fcmNotification =
                new PushPayloadHelper.FCMPayload.Notification()
                        .setTitle(channel_name)
                        .setBody(anUpdate);

        fcmPayload.setData(commonPayload);
        fcmPayload.setNotification(fcmNotification);
        pushPayloadHelper.setFcmPayload(fcmPayload);

        pushPayloadHelper.setCommonPayload(commonPayload);
        Map<String, Object> pushPayload = pushPayloadHelper.build();

        pubnub.publish().channel(channel_id).message(entryUpdate).message(pushPayload).async(
                (result, status) -> {
                    if (status.isError()) {
                        status.getErrorData().getThrowable().printStackTrace();
                    } else {
                        Log.e(TAG, ">> Submit Message >> " + result);
                        if (result != null) {
//                            Long timetoken = result.getTimetoken();
                        }
                    }
                });
    }

/*
    protected void submitUpdate(String anUpdate, String date) {
        JsonObject entryUpdate = new JsonObject();
        entryUpdate.addProperty("message", anUpdate);
        entryUpdate.addProperty("date", date);
        entryUpdate.addProperty("username", appPreferences.getuser_name());
        entryUpdate.addProperty("senderid", appPreferences.getuser_id());

        pubnub.publish().channel(channel_id).message(entryUpdate).async(
                (result, status) -> {
                    if (status.isError()) {
                        status.getErrorData().getThrowable().printStackTrace();
                    } else {
                   *//* displayMessage("[PUBLISH: sent]",
                        "timetoken: " + result.getTimetoken(),"s");*//*

                        Log.e(TAG, ">> Submit Message >> " + result);
                        if (result != null) {
                            Long timetoken = result.getTimetoken();
                            notificationSend(channel_id, anUpdate, channel_id, date);
                        }
                    }
                });
    }*/

    private void addNotificationChannel(List<String> channels) {
        String cachedToken = appPreferences.getuser_devicetoken();

        pubnub.addPushNotificationsOnChannels()
                .pushType(PNPushType.FCM)
                .deviceId(cachedToken)
                .channels(channels)
                .async((result, status) -> {
                    // Handle Response
                    if (!status.isError()) {
                        Log.e(TAG, ">> Noti >> " + result);
                        listOfRegisteredChannel();
                    }
                });
    }

    private void listOfRegisteredChannel() {
        String cachedToken = appPreferences.getuser_devicetoken();

        pubnub.auditPushChannelProvisions()
                .pushType(PNPushType.FCM)
                .deviceId(cachedToken)
                .async((result, status) -> {
                    // handle response.
                    if (!status.isError()) {
                        if (result != null) {
                            Log.e(TAG, ">> Noti Channel >> " + result.getChannels());
                        }
                    }

                });
    }

    @Override
    public void onRefresh() {
        currentPage = PAGE_START;
        isLastPage = false;
        adapter.clear();
        fetchHistory();
    }
}
