package dev.aboriginal.ui;

import androidx.appcompat.app.AppCompatActivity;

import dev.aboriginal.Models.ForgotemailCred;
import dev.aboriginal.Models.ForgotemailResponse;
import dev.aboriginal.Models.LoginCred;
import dev.aboriginal.Models.LoginResponse;
import dev.aboriginal.R;
import dev.aboriginal.network.ApiClient;
import dev.aboriginal.network.ApiInterface;
import dev.aboriginal.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ResetPassEmailActivity extends AppCompatActivity implements View.OnClickListener {
    Context context = ResetPassEmailActivity.this;
    Activity activity = ResetPassEmailActivity.this;
    EditText etEmailaddress;
Button btNext;
    ImageView imgBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass_email);
        getSupportActionBar().hide();

        init();
        clicked();
    }

    void init(){
        imgBack = findViewById(R.id.imgBack);
        etEmailaddress=findViewById(R.id.etEmailaddress);
        btNext=findViewById(R.id.btNext);
    }

    void clicked() {
        btNext.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgBack) {
            onBackPressed();
        }
        if (v.getId() == R.id.btNext) {


            if (etEmailaddress.getText().toString().length() <= 0) {
                etEmailaddress.setError("Please enter Email");
            } else if (!Utils.isValidEmaillId(etEmailaddress.getText().toString())) {
                etEmailaddress.setError("Please enter a valid email");
            }  else {
                if (!Utils.getInstance().isNetworkAvailable(getApplicationContext()))
                    noNetworkerror();
                else
                callForgotemailApi(etEmailaddress.getText().toString());

            }
        }
    }




    private void callForgotemailApi(String email) {
        // ((LoginActivity) getActivity()).setUpVisibility(progressBar, View.VISIBLE);
        Utils.getInstance().showCustomDialog(activity, "");
        ApiInterface apiService = ApiClient.getApi();
        Call<ForgotemailResponse> call = apiService.forgotEmail(new ForgotemailCred(email));
        call.enqueue(new Callback<ForgotemailResponse>() {
            @Override
            public void onResponse(Call<ForgotemailResponse> call, Response<ForgotemailResponse> response) {
                Utils.getInstance().cancelCustomDialog();
                if (response.body().getStatus()) {

                    Intent forgot = new Intent(context, RestPinActivity.class);
                    forgot.putExtra("email",email);
                    startActivity(forgot);
                   // finish();
                    // PreferenceConnector.getInstance(getActivity()).savePreferences("token", "Bearer " + response.body().getAccessToken());
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                } else {

                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                  /*  try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String s = jObjError.getString("message");
                        Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }*/
                }
            }

            @Override
            public void onFailure(Call<ForgotemailResponse> call, Throwable t) {
                Utils.getInstance().cancelCustomDialog();
              /*  ((LoginActivity) getActivity()).setUpVisibility(progressBar, View.GONE);

                t.printStackTrace();*/
            }
        });
    }
    void noNetworkerror(){
       Toast.makeText(context, "Please check internet", Toast.LENGTH_SHORT).show();
    }
}