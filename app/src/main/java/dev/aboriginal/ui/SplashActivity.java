package dev.aboriginal.ui;

import dev.aboriginal.R;
import dev.aboriginal.utils.PreferenceConnector;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Objects.requireNonNull(getSupportActionBar()).hide();

        new Thread(() -> {
            try {
                Thread.sleep(3000);
                Bundle bundle = getIntent().getExtras();
                Log.e("TAG", " >> Bundle >> " + bundle);
                if (bundle != null && bundle.getString("data") != null) {
                    Intent intent = new Intent(this, PubNubChatActivity.class);
                    intent.putExtra("FromStart", true);
                    intent.putExtra("message", bundle.getString("message"));
                    intent.putExtra("channel_id", bundle.getString("channel_id"));
                    intent.putExtra("channel_name", bundle.getString("channel_name"));
                } else {
                    if (PreferenceConnector.getInstance(SplashActivity.this).loadSavedPreferences("isChecked", "").equalsIgnoreCase("")) {
                        Intent i = new Intent(SplashActivity.this, PolicyActivity.class);
                        i.putExtra("comingfrom", "splash");
                        startActivity(i);
                        finish();

                    } else {

                        if (PreferenceConnector.getInstance(SplashActivity.this).loadSavedPreferences("isLogin", "").equalsIgnoreCase("")) {
                            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            finish();
        }).start();
    }
}
