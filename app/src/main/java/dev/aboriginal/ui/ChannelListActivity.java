package dev.aboriginal.ui;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.enums.PNPushType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import dev.aboriginal.Models.ChannelModel;
import dev.aboriginal.R;
import dev.aboriginal.chatmodule.adapters.ChannelAdapter;
import dev.aboriginal.utils.AppPreferences;
import dev.aboriginal.utils.Utils;

public class ChannelListActivity extends AppCompatActivity {
    AppPreferences appPreferences;
    RecyclerView recyclerView;
    ChannelAdapter adapter;
    private ArrayList<ChannelModel> messagesList;
    private static PubNub pubnub;
    String TAG = ChannelListActivity.class.getSimpleName();
    private ProgressDialog dialog;
    private TextView tvNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_list);
        Objects.requireNonNull(getSupportActionBar()).hide();

        appPreferences = new AppPreferences(this);

        setData();
    }

    private void setData() {
        setPubNub();
        messagesList = new ArrayList<>();
        findViewById(R.id.tvBack).setOnClickListener(view -> onBackPressed());
        recyclerView = findViewById(R.id.recycler_view);
        tvNoData = findViewById(R.id.tvNoData);
        adapter = new ChannelAdapter(this, messagesList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    private void setPubNub() {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setPublishKey("pub-c-383f4e4e-2140-4975-a491-b7aa02496a60");
        pnConfiguration.setSubscribeKey("sub-c-87d12ee0-9ea2-11eb-9adf-f2e9c1644994");
        String clientUUID = "lucky_1212";
        pnConfiguration.setUuid(clientUUID);
        pubnub = new PubNub(pnConfiguration);
    }

    private void showDialog() {
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setTitle("Please Wait!");
        dialog.setMessage("Loading....");
        dialog.show();
    }

    private void addNotificationChannel(List<String> channels) {
        String cachedToken = appPreferences.getuser_devicetoken();

        pubnub.addPushNotificationsOnChannels()
                .pushType(PNPushType.FCM)
                .deviceId(cachedToken)
                .channels(channels)
                .async((result, status) -> {
                    // Handle Response
                    if (!status.isError()) {
                        Log.e(TAG, ">> Noti >> " + result);
                        listOfRegisteredChannel();
                    }
                });
    }

    private void listOfRegisteredChannel() {
        String cachedToken = appPreferences.getuser_devicetoken();

        pubnub.auditPushChannelProvisions()
                .pushType(PNPushType.FCM)
                .deviceId(cachedToken)
                .async((result, status) -> {
                    // handle response.
                    if (!status.isError()) {
                        if (result != null) {
                            Log.e(TAG, ">> Noti Channel >> " + result.getChannels());
                        }
                    }

                });
    }

    private void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        messagesList.clear();
        showDialog();
        getChannelList();

        registerReceiver(mMessageReceiver, new IntentFilter("update_admin_channel_list"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mMessageReceiver);
    }

    @Override
    protected void onDestroy() {
        pubnub.destroy();
        super.onDestroy();
    }

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String channel_id = intent.getStringExtra("channel_id");
            String last_message = intent.getStringExtra("last_message");
            String channel_name = intent.getStringExtra("channel_name");

            boolean is_new_channel = true;

            Log.e(TAG, " update calls ");

            for (int x = 0; x < messagesList.size(); x++) {
                ChannelModel messageModel = messagesList.get(x);
                if (messageModel.id.equals(channel_id)) {
                    Log.e(TAG, " update channel : " + channel_id);
                    UpdateSingleMetaDataOfChannel(last_message, x);
                    is_new_channel = false;
                    break;
                }
            }

            if (is_new_channel) {
                Log.e(TAG, " new channel ");
                ChannelModel model = new ChannelModel(channel_name, last_message, channel_id, false);
                runOnUiThread(() -> {
                    messagesList.add(0, model);
                    adapter.notifyDataSetChanged();
                });
            }
        }
    };

    private void UpdateSingleMetaDataOfChannel(String last_message, int update_position) {
        messagesList.get(update_position).setMessage(last_message);
        messagesList.get(update_position).setRead(false);

        // move item to the top
        ChannelModel removedItem = messagesList.get(update_position);
        messagesList.remove(update_position);  // remove item from the list
        messagesList.add(0, removedItem); // add at 0 index of your list

        adapter.notifyDataSetChanged();
    }

    public static void updatePushNotificationsOnChannels(String[] channels, String deviceToken, String oldToken) {
        if (oldToken != null) {
            pubnub.removeAllPushNotificationsFromDeviceWithPushToken()
                    .pushType(PNPushType.FCM)
                    .deviceId(oldToken)
                    .async((result, status) -> {
                        // Handle Response
                    });
        }

        pubnub.addPushNotificationsOnChannels()
                .pushType(PNPushType.FCM)
                .deviceId(deviceToken)
                .channels(Arrays.asList(channels))
                .async((result, status) -> {
                    // Handle Response
                });
    }

    private void getChannelList() {

        pubnub.listChannelsForChannelGroup()
                .channelGroup(Utils.GROUPCHANNELNAME)
                .async((result, status) -> {

                    if (status.isError()) {
                        dismissDialog();
                        recyclerView.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        Log.e(TAG, ">> Error >> " + status.getErrorData());
                    } else {
                        if (result != null && result.getChannels().size() > 0) {
                            getMetadataFromChannel(result.getChannels());

                            numberOfMessages(result.getChannels());
                            addNotificationChannel(result.getChannels());
                        } else {
                            dismissDialog();
                            recyclerView.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }

    private void numberOfMessages(List<String> channel) {
        Long lastHourTimetoken = (Calendar.getInstance().getTimeInMillis() - TimeUnit.MINUTES.toMillis(5)) * 10000L;

        pubnub.messageCounts()
                .channels(channel)
                .channelsTimetoken(Collections.singletonList(lastHourTimetoken))
                .async((result, status) -> {
                    if (!status.isError()) {
                        if (result != null) {
                            for (Map.Entry<String, Long> entry : result.getChannels().entrySet()) {
                                entry.getKey(); // the channel name
                                entry.getValue(); // number of messages for that channel

                                Log.e(TAG, "Messages >> " + entry.getValue() + " >> Name >> " + entry.getKey());
                            }
                        }
                    } else {
                        status.getErrorData().getThrowable().printStackTrace();
                    }
                });
    }

    /*private void fetchHistory(String channel_id) {
        pubnub.history()
                .channel(channel_id) // where to fetch history from
                .count(1) // how many items to fetch
                .includeTimetoken(true)
                .async((result, status) -> new Thread(() -> {
                    Log.e(TAG, ">> result 11 >>. " + result);
                    dismissDialog();
                    if (!status.isError()) {
                        if (result != null) {
                            if (!status.isError() && !result.getMessages().isEmpty()) {

                                for (int i = 0; i < result.getMessages().size(); i++) {
                                    String msj = "";
                                    if (result.getMessages().get(i).getEntry().getAsJsonObject().get("message") != null) {
                                        msj = result.getMessages().get(i).getEntry().getAsJsonObject().get("message").getAsString();
                                    }

                                    ChannelModel model = new ChannelModel(channel_id, msj);
                                    runOnUiThread(() -> {
                                        messagesList.add(model);
                                        adapter.notifyDataSetChanged();
                                    });
                                }
                            }
                        }
                    } else {
                        status.getErrorData().getThrowable().printStackTrace();
                    }
                }).start());
    }

    private void getAllChannelMetadata() {
        pubnub.getAllChannelsMetadata()
                .includeCustom(true)
                .async((result, status) -> {
                    if (status.isError()) {
                        Log.e(TAG, ">> Error Metadata >> " + status.getErrorData());
                    } else {

                        List<String> channels = new ArrayList<>();
                        if (result != null) {
                            Log.e(TAG, ">> List Metadata >> " + result.getData());
                            for (int i = 0; i < result.getData().size(); i++) {
                                if (result.getData().get(i).getCustom() != null) {
                                    JsonObject object = (JsonObject) result.getData().get(i).getCustom();
                                    String message = "";
                                    if (object.get("last_message") != null) {
                                        message = object.get("last_message").getAsString();
                                    }
                                    Log.e(TAG, ">>Message >> " + message);
                                    String channel_id = object.get("channel_id").getAsString();
                                    Log.e(TAG, ">>channel_id >> " + channel_id);
                                    channels.add(channel_id);
                                    ChannelModel model = new ChannelModel(channel_id, message);
                                    runOnUiThread(() -> {
                                        messagesList.add(model);
                                        adapter.notifyDataSetChanged();
                                    });
                                }
                            }

                            if (channels.size() > 0) {
                                numberOfMessages(channels);
                                addNotificationChannel(channels);
                            }

                        }
                    }
                });
    }*/

    private void getMetadataFromChannel(List<String> key) {
        for (int i = 0; i < key.size(); i++) {

            pubnub.getChannelMetadata()
                    .channel(key.get(i))
                    .includeCustom(true)
                    .async((result, status) -> {
                        dismissDialog();
                        if (status.isError()) {
                            recyclerView.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                            Log.e(TAG, ">> Error Metadata >> " + status.getErrorData());
                        } else {
                            if (result != null) {
                                tvNoData.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                Log.e(TAG, ">> key >> " + result.getData());

                                String message = "";
                                boolean isRead = false;
                                JsonObject object = (JsonObject) result.getData().getCustom();
                                if (object != null) {

                                    if (object.has("message")) {
                                        if (object.get("message") != null) {
                                            message = object.get("message").getAsString();
                                        }
                                    }

                                    if (object.get("isRead") != null) {
                                        isRead = object.get("isRead").getAsBoolean();
                                    }
                                }

                                String channel_name = result.getData().getName();
                                String channel_id = result.getData().getId();

                                ChannelModel model = new ChannelModel(channel_name, message, channel_id, isRead);
                                runOnUiThread(() -> {
                                    messagesList.add(model);
                                    adapter.notifyDataSetChanged();
                                });
                            } else {
                                recyclerView.setVisibility(View.GONE);
                                tvNoData.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}