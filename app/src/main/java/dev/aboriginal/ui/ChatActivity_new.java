package dev.aboriginal.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.objects_api.channel.PNChannelMetadataResult;
import com.pubnub.api.models.consumer.objects_api.membership.PNMembershipResult;
import com.pubnub.api.models.consumer.objects_api.uuid.PNUUIDMetadataResult;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.pubnub.api.models.consumer.pubsub.PNSignalResult;
import com.pubnub.api.models.consumer.pubsub.files.PNFileEventResult;
import com.pubnub.api.models.consumer.pubsub.message_actions.PNMessageActionResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import dev.aboriginal.CustomAdapter;
import dev.aboriginal.Models.MessageModel;
import dev.aboriginal.R;

public class ChatActivity_new extends AppCompatActivity implements View.OnClickListener {
    private EditText entryUpdateText;
    private TextView messagesText;
    TextView tvBack;
    private PubNub pubnub;
    final private String clientUUID = "lucky_1212";
    private String theChannel = "ACP_Test";
    private String theEntry = "A30";
    CustomAdapter adapter;
    RecyclerView recyclerView;
    private RecyclerView.OnScrollListener mOnScrollListener;
    ArrayList<MessageModel> messagesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatnew);

        Objects.requireNonNull(getSupportActionBar()).hide();

        tvBack = findViewById(R.id.tvBack);
        tvBack.setOnClickListener(v -> onBackPressed());

        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setPublishKey("pub-c-383f4e4e-2140-4975-a491-b7aa02496a60");
        pnConfiguration.setSubscribeKey("sub-c-87d12ee0-9ea2-11eb-9adf-f2e9c1644994");
        pnConfiguration.setUuid(clientUUID);

        pubnub = new PubNub(pnConfiguration);

        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void message(PubNub pubnub, PNMessageResult event) {
                JsonObject message = event.getMessage().getAsJsonObject();
                String entryVal = message.get("username").getAsString();
                String updateVal = message.get("message").getAsString();
                String senderid = message.get("senderid").getAsString();
                String messageid = message.get("messageid").getAsString();


                displayMessage("[MESSAGE: received]", entryVal + ": " + updateVal, entryVal);


            }

            @Override
            public void status(PubNub pubnub, PNStatus event) {
              /*  displayMessage("[STATUS: " + event.getCategory() + "]",
                    "connected to channels: " + event.getAffectedChannels(),"");

                if (event.getCategory().equals(PNStatusCategory.PNConnectedCategory)){
                    submitUpdate(theEntry, "Harmless.");
                }*/
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult event) {
                displayMessage("[PRESENCE: " + event.getEvent() + ']',
                        "uuid: " + event.getUuid() + ", channel: " + event.getChannel(), "");
            }

            // even if you don't need these callbacks, you still have include them
            // because we are extending an Abstract class
            @Override
            public void signal(PubNub pubnub, PNSignalResult event) {
            }

            @Override
            public void uuid(PubNub pubnub, PNUUIDMetadataResult pnUUIDMetadataResult) {
            }

            @Override
            public void channel(PubNub pubnub, PNChannelMetadataResult pnChannelMetadataResult) {
            }

            @Override
            public void membership(PubNub pubnub, PNMembershipResult event) {
            }

            @Override
            public void messageAction(PubNub pubnub, PNMessageActionResult event) {
            }

            @Override
            public void file(PubNub pubnub, PNFileEventResult event) {
            }
        });

        pubnub.subscribe()
                .channels(Arrays.asList(theChannel))
                .withPresence()
                .execute();

        entryUpdateText = findViewById(R.id.entry_update_text);
        messagesText = findViewById(R.id.messages_text);


        // Populate dummy messages in List, you can implement your code here
        messagesList = new ArrayList<>();
     /*   for (int i=0;i<10;i++) {
            messagesList.add(new MessageModel("Hi", i % 2 == 0 ? CustomAdapter.MESSAGE_TYPE_IN : CustomAdapter.MESSAGE_TYPE_OUT));
        }*/

        adapter = new CustomAdapter(this, messagesList);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    protected void submitUpdate(String anEntry, String anUpdate) {
        JsonObject entryUpdate = new JsonObject();
        entryUpdate.addProperty("username", anEntry);
        entryUpdate.addProperty("message", anUpdate);
        entryUpdate.addProperty("senderid", "456789");
        entryUpdate.addProperty("messageid", "12345");


        pubnub.publish().channel(theChannel).message(entryUpdate).async(
                (result, status) -> {
                    if (status.isError()) {
                        status.getErrorData().getThrowable().printStackTrace();
                    } else {
                   /* displayMessage("[PUBLISH: sent]",
                        "timetoken: " + result.getTimetoken(),"s");*/

                        adapter.notifyDataSetChanged();
                    }
                });
    }

    protected void displayMessage(String messageType, String aMessage, String type) {
        String newLine = "\n";

        final StringBuilder textBuilder = new StringBuilder()
                /*   .append(messageType)
                   .append(newLine)*/
                .append(aMessage)
                .append(newLine)
                .append(messagesText.getText().toString());


        if (theEntry.equals(type)) {
            messagesList.add(new MessageModel(aMessage,"", CustomAdapter.MESSAGE_TYPE_OUT));
        } else {
            messagesList.add(new MessageModel(aMessage,"", CustomAdapter.MESSAGE_TYPE_IN));
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                messagesText.setText(textBuilder.toString());


                adapter.notifyDataSetChanged();


            }
        });
    }

    @Override
    public void onClick(View view) {
        submitUpdate(theEntry, entryUpdateText.getText().toString());
        entryUpdateText.setText("");
    }


   /* private void initializeScrollListener() {
        mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstCompletelyVisibleItemPosition =
                        ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

                if (firstCompletelyVisibleItemPosition == History.TOP_ITEM_OFFSET && dy < 0) {
                    fetchHistory();
                }
            }
        };
    }



    private void fetchHistory() {
        if (History.isLoading()) {
            return;
        }
        History.setLoading(true);
        mSwipeRefreshLayout.setRefreshing(true);
        History.getAllMessages(hostActivity.getPubNub(), mChannel, getEarliestTimestamp(),
                new History.CallbackSkeleton() {
                    @Override
                    public void handleResponse(List<Message> newMessages) {
                        if (!newMessages.isEmpty()) {
                            mMessages.addAll(0, newMessages);
                            History.chainMessages(mMessages, mMessages.size());
                            runOnUiThread(() -> mChatAdapter.update(mMessages));
                        } else if (mMessages.isEmpty()) {
                            runOnUiThread(() -> mEmptyView.setVisibility(View.VISIBLE));
                        } else {
                            runOnUiThread(() -> Toast.makeText(fragmentContext, getString(R.string.no_more_messages),
                                    Toast.LENGTH_SHORT).show());
                        }
                        runOnUiThread(() -> {
                            mSwipeRefreshLayout.setRefreshing(false);
                            Log.d("new_arrival", "size: " + mMessages.size());
                        });
                        History.setLoading(false);
                    }
                });
    }*/
}
