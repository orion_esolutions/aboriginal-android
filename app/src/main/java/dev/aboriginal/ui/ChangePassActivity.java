package dev.aboriginal.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import dev.aboriginal.Models.ResetPasswordResponse;
import dev.aboriginal.Models.ResetPawordCred;
import dev.aboriginal.R;
import dev.aboriginal.network.ApiClient;
import dev.aboriginal.network.ApiInterface;
import dev.aboriginal.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassActivity extends AppCompatActivity implements View.OnClickListener {

    Context context = ChangePassActivity.this;
    Activity activity = ChangePassActivity.this;
    EditText etNewpassword,etReconfirmpassword;
    Button btResetpassword;
String emailString,otpString,newpasswrdString;
ImageView imgBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        Objects.requireNonNull(getSupportActionBar()).hide();

        init();
        clicked();
    }

    void init(){

        Intent intent = getIntent();
        emailString = intent.getStringExtra("email");
        otpString = intent.getStringExtra("otp");


        imgBack=findViewById(R.id.imgBack);
        etNewpassword=findViewById(R.id.etNewpassword);

        etReconfirmpassword=findViewById(R.id.etReconfirmpassword);
        btResetpassword=findViewById(R.id.btResetpassword);
    }

    void clicked() {
        btResetpassword.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgBack) {
            onBackPressed();
        }
        if (v.getId() == R.id.btResetpassword) {

            if (etNewpassword.getText().toString().length() <= 0) {
                etNewpassword.setError("Please enter your password");
            } else if (!Utils.isValidPassword(etNewpassword.getText().toString())) {
                etNewpassword.setError("Passwords must contain a minimum of 8 characters, at least 1 Alphabet, 1 Number and 1 Special Character. Special Character includes only (!,@,#,$,%,^,&,*,=,+,<,.,>,-,_)");
            }  else if (etReconfirmpassword.getText().toString().length() <= 0) {
                etReconfirmpassword.setError("Please confirm your password");
            } else if (!etReconfirmpassword.getText().toString().equals(etNewpassword.getText().toString())) {
                etReconfirmpassword.setError("Password and confirm password does not match");
            } else {

                if (!Utils.getInstance().isNetworkAvailable(getApplicationContext()))
                    noNetworkerror();
                else

                    callchangepawrdApi();
            }


        }
    }



    private void callchangepawrdApi() {
        newpasswrdString=etNewpassword.getText().toString();
        // ((LoginActivity) getActivity()).setUpVisibility(progressBar, View.VISIBLE);
        Utils.getInstance().showCustomDialog(activity, "");
        ApiInterface apiService = ApiClient.getApi();

        Call<ResetPasswordResponse> call = apiService.ResetPassword(new ResetPawordCred(emailString,otpString,newpasswrdString));
        call.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(@NotNull Call<ResetPasswordResponse> call, @NotNull Response<ResetPasswordResponse> response) {
                Utils.getInstance().cancelCustomDialog();
                if (response.body() != null) {
                    if (response.body().getStatus()) {
                        Intent forgot = new Intent(context,LoginActivity.class);
                        startActivity(forgot);
                         finish();
                       Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                    } else {

                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResetPasswordResponse> call, @NotNull Throwable t) {
                Utils.getInstance().cancelCustomDialog();
            }
        });
    }


    void noNetworkerror(){
        //    Utils.getInstance().showWarningErrorMessage(getResources().getString(R.string.warning), getResources().getString(R.string.NoNaetwork), getResources().getString(R.string.ok), SigninOtp.this);
        Toast.makeText(context, "Please check internet", Toast.LENGTH_SHORT).show();
    }
}