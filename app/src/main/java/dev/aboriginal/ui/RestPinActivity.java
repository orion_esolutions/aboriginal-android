package dev.aboriginal.ui;

import androidx.appcompat.app.AppCompatActivity;

import dev.aboriginal.Models.ForgotemailCred;
import dev.aboriginal.Models.ForgotemailResponse;
import dev.aboriginal.Models.OtpCred;
import dev.aboriginal.Models.OtpResponse;
import dev.aboriginal.R;
import dev.aboriginal.network.ApiClient;
import dev.aboriginal.network.ApiInterface;
import dev.aboriginal.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class RestPinActivity extends AppCompatActivity implements View.OnClickListener, View.OnKeyListener {
    Context context = RestPinActivity.this;
    Activity activity = RestPinActivity.this;
    Button btNextPin;
    String token = "", emailString;
    EditText etPin1, etPin2, etPin3, etPin4, etPin5, etPin6;
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_pin);

        getSupportActionBar().hide();

        init();
        clicked();

        setOnKeyListener();
        textWatcher();
    }

    void init() {

        Intent intent = getIntent();
        emailString = intent.getStringExtra("email");

        imgBack = findViewById(R.id.imgBack);
        btNextPin = findViewById(R.id.btNextPin);

        etPin1 = findViewById(R.id.etPin1);
        etPin2 = findViewById(R.id.etPin2);
        etPin3 = findViewById(R.id.etPin3);
        etPin4 = findViewById(R.id.etPin4);
        etPin5 = findViewById(R.id.etPin5);
        etPin6 = findViewById(R.id.etPin6);
    }

    void clicked() {
        btNextPin.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }


    private void setOnKeyListener() {
        etPin2.setOnKeyListener(this);
        etPin3.setOnKeyListener(this);
        etPin4.setOnKeyListener(this);
        etPin5.setOnKeyListener(this);
        etPin6.setOnKeyListener(this);
    }

    private void textWatcher() {
        etPin1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int length) {
                if (!etPin1.getText().toString().isEmpty() && !etPin2.getText().toString().isEmpty() && !etPin3.getText().toString().isEmpty() && !etPin4.getText().toString().isEmpty() && !etPin5.getText().toString().isEmpty() && !etPin6.getText().toString().isEmpty()) {

                    if (!Utils.getInstance().isNetworkAvailable(getApplicationContext())) {
                        noNetworkerror();
                    }


                } else {
                    if (length == 1) {
                        etPin2.requestFocus();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etPin2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int length) {
                if (!etPin1.getText().toString().isEmpty() && !etPin2.getText().toString().isEmpty() && !etPin3.getText().toString().isEmpty() && !etPin4.getText().toString().isEmpty() && !etPin5.getText().toString().isEmpty() && !etPin6.getText().toString().isEmpty()) {
                    if (!Utils.getInstance().isNetworkAvailable(getApplicationContext())) {
                        noNetworkerror();
                    }
                } else {
                    if (length == 1) {
                        etPin3.requestFocus();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etPin3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int length) {
                if (!etPin1.getText().toString().isEmpty() && !etPin2.getText().toString().isEmpty() && !etPin3.getText().toString().isEmpty() && !etPin4.getText().toString().isEmpty() && !etPin5.getText().toString().isEmpty() && !etPin6.getText().toString().isEmpty()) {
                    if (!Utils.getInstance().isNetworkAvailable(getApplicationContext())) {
                        noNetworkerror();
                    }
                } else {
                    if (length == 1) {
                        etPin4.requestFocus();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etPin4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int length) {
                if (!etPin1.getText().toString().isEmpty() && !etPin2.getText().toString().isEmpty() && !etPin3.getText().toString().isEmpty() && !etPin4.getText().toString().isEmpty() && !etPin5.getText().toString().isEmpty() && !etPin6.getText().toString().isEmpty()) {
                    if (!Utils.getInstance().isNetworkAvailable(getApplicationContext())) {
                        noNetworkerror();
                    }
                } else {
                    if (length == 1) {
                        etPin5.requestFocus();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etPin5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int length) {
                if (!etPin1.getText().toString().isEmpty() && !etPin2.getText().toString().isEmpty() && !etPin3.getText().toString().isEmpty() && !etPin4.getText().toString().isEmpty() && !etPin5.getText().toString().isEmpty() && !etPin6.getText().toString().isEmpty()) {
                    if (!Utils.getInstance().isNetworkAvailable(getApplicationContext())) {
                        noNetworkerror();
                    }
                } else {
                    if (length == 1) {
                        etPin6.requestFocus();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etPin6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int length) {
                if (length == 1) {
                    if (!etPin1.getText().toString().isEmpty() && !etPin2.getText().toString().isEmpty() && !etPin3.getText().toString().isEmpty() && !etPin4.getText().toString().isEmpty() && !etPin5.getText().toString().isEmpty() && !etPin6.getText().toString().isEmpty()) {
                        if (!Utils.getInstance().isNetworkAvailable(getApplicationContext())) {
                            noNetworkerror();
                        } else {

                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        switch (view.getId()) {

            case R.id.etPin2:
                if (keyCode == KeyEvent.KEYCODE_DEL && etPin2.getText().toString().trim().isEmpty() && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    etPin1.requestFocus();
                }
                break;
            case R.id.etPin3:
                if (keyCode == KeyEvent.KEYCODE_DEL && etPin3.getText().toString().trim().isEmpty() && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    etPin2.requestFocus();
                }

                break;
            case R.id.etPin4:
                if (keyCode == KeyEvent.KEYCODE_DEL && etPin4.getText().toString().isEmpty() && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    etPin3.requestFocus();
                }
                break;

            case R.id.etPin5:
                if (keyCode == KeyEvent.KEYCODE_DEL && etPin4.getText().toString().isEmpty() && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    etPin4.requestFocus();
                }
                break;
            case R.id.etPin6:
                if (keyCode == KeyEvent.KEYCODE_DEL && etPin4.getText().toString().isEmpty() && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    etPin5.requestFocus();
                }
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgBack) {
            onBackPressed();
        }
        if (v.getId() == R.id.btNextPin) {

            if (!Utils.getInstance().isNetworkAvailable(getApplicationContext())) {
                noNetworkerror();
            } else {
                token = etPin1.getText().toString() + etPin2.getText().toString() + etPin3.getText().toString() + etPin4.getText().toString() + etPin5.getText().toString() + etPin6.getText().toString();
                if (token.length() == 6) {
                    callOTPApi(token);
                } else {
                    Toast.makeText(context, "Enter vaild otp", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    private void callOTPApi(String otp) {
        Utils.getInstance().showCustomDialog(activity, "");
        ApiInterface apiService = ApiClient.getApi();

        Call<OtpResponse> call = apiService.otpCheck(new OtpCred(emailString, otp));
        call.enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {
                Utils.getInstance().cancelCustomDialog();
                if (response.body().getStatus()) {

                    Intent changePassActivity = new Intent(context, ChangePassActivity.class);
                    changePassActivity.putExtra("email", emailString);
                    changePassActivity.putExtra("otp", otp);
                    startActivity(changePassActivity);
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                } else {

                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                Utils.getInstance().cancelCustomDialog();
              /*  ((LoginActivity) getActivity()).setUpVisibility(progressBar, View.GONE);

                t.printStackTrace();*/
            }
        });
    }


    void noNetworkerror() {
        Toast.makeText(context, "Please check internet", Toast.LENGTH_SHORT).show();
    }

}