package dev.aboriginal.ui;

import dev.aboriginal.R;
import dev.aboriginal.utils.PreferenceConnector;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PolicyActivity extends AppCompatActivity {

  LinearLayout okay_btn,okay_lay;
  CheckBox checkbox;
TextView tvBack;
String comingfrom="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_policy_screen);
        Intent intent = getIntent();
        comingfrom = intent.getStringExtra("comingfrom");

        checkbox=(CheckBox)findViewById(R.id.checkbox);
        okay_btn=(LinearLayout)findViewById(R.id.okay_btn);
        okay_lay=findViewById(R.id.okay_lay);
        tvBack=findViewById(R.id.tvBack);


        if(comingfrom.equals("splash")){
            tvBack.setVisibility(View.GONE);
            okay_lay.setVisibility(View.VISIBLE);
        }else {
            tvBack.setVisibility(View.VISIBLE);
            okay_lay.setVisibility(View.GONE);
        }

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        okay_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkbox.isChecked()){

                    PreferenceConnector.getInstance(PolicyActivity.this).savePreferences("isChecked","Yes");
                }
                startActivity(new Intent(PolicyActivity.this, LoginActivity.class));
                finish();
            }
        });
        Log.e("string","String");


    }
}
