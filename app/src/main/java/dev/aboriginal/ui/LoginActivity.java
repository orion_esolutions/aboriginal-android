package dev.aboriginal.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import dev.aboriginal.Models.LoginCred;
import dev.aboriginal.Models.LoginResponse;
import dev.aboriginal.R;
import dev.aboriginal.network.ApiClient;
import dev.aboriginal.network.ApiInterface;
import dev.aboriginal.utils.AppPreferences;
import dev.aboriginal.utils.PreferenceConnector;
import dev.aboriginal.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Context context = LoginActivity.this;
    Activity activity = LoginActivity.this;
    TextView tvForgotpass;
    EditText etEmail, etPassword;
    Button btLogin, btGuest;
    private ApiInterface apiService;
    AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Objects.requireNonNull(getSupportActionBar()).hide();

        appPreferences = new AppPreferences(this);

        init();
        clicked();
    }

    void init() {
        apiService = ApiClient.getApi();
        tvForgotpass = findViewById(R.id.tvForgotpass);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btLogin = findViewById(R.id.btLogin);
        btGuest = findViewById(R.id.btGuest);
    }

    void clicked() {
        tvForgotpass.setOnClickListener(this);
        btLogin.setOnClickListener(this);
        btGuest.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvForgotpass) {
            Intent forgot = new Intent(context, ResetPassEmailActivity.class);
            startActivity(forgot);
        }
        if (v.getId() == R.id.btGuest) {
            Intent forgot = new Intent(context, HomeActivity.class);
            startActivity(forgot);
            finish();
        } else if (v.getId() == R.id.btLogin) {
            if (etEmail.getText().toString().length() <= 0) {
                etEmail.setError("Please enter Email");
            } else if (!Utils.isValidEmaillId(etEmail.getText().toString())) {
                etEmail.setError("Please enter a valid email");
            } else if ((etPassword.getText().toString().length() <= 0)) {
                etPassword.setError("Please enter Password");
            } else {
                LoginCred mLogin = new LoginCred();
                mLogin.setUsername(etEmail.getText().toString());
                mLogin.setPassword(etPassword.getText().toString());
                callLoginApi(mLogin);
            }
        }
    }


    private void callLoginApi(LoginCred loginCred) {
        // ((LoginActivity) getActivity()).setUpVisibility(progressBar, View.VISIBLE);
        Utils.getInstance().showCustomDialog(activity, "");
        Call<LoginResponse> call = apiService.Login(loginCred);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NotNull Call<LoginResponse> call, @NotNull Response<LoginResponse> response) {
                Utils.getInstance().cancelCustomDialog();
                Log.e("loginresponse", new Gson().toJson(response.body()));
//                {"loginUserDetails":{"UserDetails":{"Email":"john@yopmail.com","FirstName":"John","Id":22,"isPatient":false,"LastName":"Jackson ","MobileNumber":"9876785643","UserName":"john_22"}},"Message":"Success","status":true}
//                {"loginUserDetails":{"UserDetails":{"Email":"PatientTeam1@yopmail.com","FirstName":"PatientTeam1","Id":20,"isPatient":true,"LastName":"PatientTeam1","MobileNumber":"969585858858","UserName":"PatientTeam1"}},"Message":"Success","status":true}
                if (response.body() != null) {
                    if (response.body().getStatus()) {

                        appPreferences.setuser_id(String.valueOf(response.body().getLoginUserDetails().getUserDetails().getId()));
                        appPreferences.setuser_name(response.body().getLoginUserDetails().getUserDetails().getUserName());
                        appPreferences.setuser_first_name(response.body().getLoginUserDetails().getUserDetails().getFirstName().toString());
                        appPreferences.setuser_last_name(response.body().getLoginUserDetails().getUserDetails().getLastName().toString());
                        appPreferences.setuser_is_admin(response.body().getLoginUserDetails().getUserDetails().getPatient());

                        Intent forgot = new Intent(context, HomeActivity.class);
                        startActivity(forgot);
                        finish();
                        PreferenceConnector.getInstance(LoginActivity.this).savePreferences("isLogin", "Yes");
                        // PreferenceConnector.getInstance(getActivity()).savePreferences("token", "Bearer " + response.body().getAccessToken());
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                    } else {

                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                  /*  try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String s = jObjError.getString("message");
                        Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }*/
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<LoginResponse> call, @NotNull Throwable t) {
                Utils.getInstance().cancelCustomDialog();
              /*  ((LoginActivity) getActivity()).setUpVisibility(progressBar, View.GONE);

                t.printStackTrace();*/
            }
        });
    }


}