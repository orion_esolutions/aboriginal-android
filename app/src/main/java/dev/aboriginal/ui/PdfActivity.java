package dev.aboriginal.ui;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.link.LinkHandler;
import com.github.barteksc.pdfviewer.model.LinkTapEvent;

import java.util.Objects;

import dev.aboriginal.R;

//public class PdfActivity extends AppCompatActivity implements OnPageChangeListener, OnLoadCompleteListener {
public class PdfActivity extends AppCompatActivity implements LinkHandler {
    TextView tvBack;
    PDFView pdfView;
    WebView webView;

    private static final String TAG = PdfActivity.class.getSimpleName();
    public static final String SAMPLE_FILE = "workbook_jan21.pdf";

    Integer pageNumber = 0;
    String pdfFileName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        Objects.requireNonNull(getSupportActionBar()).hide();

        tvBack=findViewById(R.id.tvBack);
        tvBack.setOnClickListener(v -> onBackPressed());
        pdfView=findViewById(R.id.pdfv);
          pdfView.fromAsset("workbook_jan21.pdf").linkHandler(this).load();
      //  displayFromAsset(SAMPLE_FILE);







     /*   File pdfFile = new File("file://android_asset/workbook_jan21.pdf");
        Uri path = Uri.fromFile(pdfFile);
        Intent intent = new Intent(Intent.CATEGORY_OPENABLE);
        intent.setDataAndType(path, "application/pdf");
        startActivity(intent);*/



      /*  webView=findViewById(R.id.webvieww);
    *//*    pdfView=findViewById(R.id.pdfv);
      //  pdfView.fromAsset("workbook_jan21.pdf").load();
        displayFromAsset(SAMPLE_FILE);*//*



        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);



            settings.setAllowFileAccessFromFileURLs(true);
            settings.setAllowUniversalAccessFromFileURLs(true);

        settings.setBuiltInZoomControls(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl("file:///android_asset/workbook_jan21.pdf");


      *//*  WebView webview = new WebView( this ) ;
        webview.getSettings().setJavaScriptEnabled( true ) ;
        setContentView(webview) ;
        String pdf =
                "http://www.adobe.com/devnet/acrobat/pdfs/pdf_open_parameters.pdf" ;
        webview.loadUrl( "http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf) ;*//*

*/
    }

    @Override
    public void handleLinkEvent(LinkTapEvent event) {

        String uri = event.getLink().getUri();
        Integer page = event.getLink().getDestPageIdx();
        if (uri != null && !uri.isEmpty()) {
            handleUri(uri);
        } else if (page != null) {
            handlePage(page);
        }

    }

    @SuppressLint("QueryPermissionsNeeded")
    private void handleUri(String uri) {
        Log.i("link_url",uri);
        Uri parsedUri = Uri.parse(uri);
        Intent intent = new Intent(Intent.ACTION_VIEW, parsedUri);
        Context context = pdfView.getContext();
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        } else {
            Intent intent2 = new Intent(Intent.ACTION_VIEW,parsedUri);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.setPackage("com.android.chrome");
            try {
                context.startActivity(intent2);
            } catch (ActivityNotFoundException ex) {
                // Chrome browser presumably not installed so allow user to choose instead
                intent2.setPackage(null);
                context.startActivity(intent2);
            }

            
            Log.w(TAG, "No activity found for URI: " + uri);
        }
    }

    private void handlePage(int page) {
        pdfView.jumpTo(page);
    }

  /*  private void displayFromAsset(String assetFileName) {
        pdfFileName = assetFileName;

        pdfView.fromAsset(SAMPLE_FILE)
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");
    }


    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }*/
}