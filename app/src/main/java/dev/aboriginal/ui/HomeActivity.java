package dev.aboriginal.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import adapter.DrawerAdapter;
import data.tables.SubTabs;
import data.tables.UpperTabs;
import de.greenrobot.event.EventBus;
import dev.aboriginal.Models.DrawerModel;
import dev.aboriginal.Models.TabModel;
import dev.aboriginal.OnResult;
import dev.aboriginal.R;
import dev.aboriginal.WebServices.GetMethod;
import dev.aboriginal.controller.ApplicationController;
import dev.aboriginal.network.ApiClient;
import dev.aboriginal.network.ApiInterface;
import dev.aboriginal.utils.AppPreferences;
import dev.aboriginal.utils.Constants;
import dev.aboriginal.utils.Utils;
import fragment.navigation.FragmentNavigationManager;
import fragment.navigation.NavigationManager;
import retrofit2.Call;

public class HomeActivity extends AppCompatActivity implements OnResult {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private final ArrayList<String> Tabs = new ArrayList<>();
    DrawerAdapter drawerAdapter;
    //    private ExpandableListView mExpandableListView;
//    private ExpandableListAdapter mExpandableListAdapter;
    private List<String> mExpandableListTitle;
    private NavigationManager mNavigationManager;
    ArrayList<DrawerModel.Result1.IstTabs> drawerModels = new ArrayList<>();
    ArrayList<TabModel> upperTabs = new ArrayList<>();
    ArrayList<TabModel> subTabs = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextView mToolbar;
    LinearLayout drawer_icon;
    ImageView searchView, cross_btn, refresh;
    LinearLayout search_view_lay;
    int upperPosition = 0;
    int subTabPosition = 0;
    boolean isOpened;
    EditText search_text;
    RecyclerView.SmoothScroller smoothScroller;
    RecyclerView.LayoutManager mLayoutManager;

    public HomeActivity() {
    }

    AppPreferences preferences;

    @SuppressLint("RtlHardcoded")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Objects.requireNonNull(getSupportActionBar()).hide();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mToolbar = findViewById(R.id.toolbar);
        drawer_icon = findViewById(R.id.drawer_icon);
        searchView = findViewById(R.id.search_view);
        cross_btn = findViewById(R.id.cross_btn);
        refresh = findViewById(R.id.refresh);
        search_text = findViewById(R.id.search_text);
        search_view_lay = findViewById(R.id.search_view_lay);

        preferences = new AppPreferences(this);
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Log.e("TAG", "Fetching FCM registration token failed", task.getException());
                return;
            }
            preferences.setuser_devicetoken(task.getResult());
            Log.e("TAG", "Token >> " + task.getResult());
        });

        searchView.setOnClickListener(view -> {
            mToolbar.setVisibility(View.GONE);
            drawer_icon.setVisibility(View.VISIBLE);
            search_view_lay.setVisibility(View.VISIBLE);
            searchView.setVisibility(View.GONE);
            refresh.setVisibility(View.GONE);
            search_text.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(search_text, InputMethodManager.SHOW_IMPLICIT);
            isOpened = false;
        });

        refresh.setOnClickListener(view -> getDrawerItemsCurrentDate("loading..."));
        search_text.getText().clear();
        Log.i("touch11", "search134");
        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String text = search_text.getText().toString();

//                onResult("Hello StackOverflow From BNK!");
                final String title = upperTabs.get(upperPosition).getTabName();
                final Handler handler = new Handler();
                handler.postDelayed(() -> runOnUiThread(() -> mNavigationManager.showFragmentAction(title, String.valueOf(subTabPosition), String.valueOf(upperPosition), text)), 100);

            }
        });

        cross_btn.setOnClickListener(view -> {
            // hideSoftKeyboard(MainActivity.this);
            final Handler handler = new Handler();
            handler.postDelayed(() -> runOnUiThread(() -> {


                search_view_lay.setVisibility(View.GONE);
                searchView.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                mToolbar.setVisibility(View.VISIBLE);
                search_text.getText().clear();
                Log.i("touch11", "search191");
                if (!upperTabs.isEmpty()) {
                    String title = upperTabs.get(upperPosition).getTabName();
                    mNavigationManager.showFragmentAction(title, String.valueOf(subTabPosition), String.valueOf(upperPosition), "");

                }
            }), 1000);

        });


//        mExpandableListView = (ExpandableListView) findViewById(R.id.navList);

        mNavigationManager = FragmentNavigationManager.obtain(this);

        recyclerView = findViewById(R.id.navList);

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        smoothScroller = new
                LinearSmoothScroller(HomeActivity.this) {
                    @Override
                    protected int getVerticalSnapPreference() {
                        return LinearSmoothScroller.SNAP_TO_START;
                    }
                };
        //   OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

        drawer_icon.setOnClickListener(view -> {
            mToolbar.setVisibility(View.VISIBLE);
            search_view_lay.setVisibility(View.GONE);
            searchView.setVisibility(View.VISIBLE);
            mDrawerLayout.openDrawer(Gravity.LEFT);
            //  hideSoftKeyboard(MainActivity.this);


        });


        LayoutInflater inflater = getLayoutInflater();

        @SuppressLint("InflateParams") View listHeaderView = inflater.inflate(R.layout.nav_header, null, false);

//        mExpandableListView.addHeaderView(listHeaderView);

        setupDrawer();

        upperTabs = UpperTabs.getInstance().getSizes();

        Log.i("size", String.valueOf(upperTabs.size() + 1));

        if (!upperTabs.isEmpty()) {
            Collections.sort(upperTabs, (fruit2, fruit1) -> {
                int i = Integer.parseInt(fruit1.getSorting());
                int j = Integer.parseInt(fruit2.getSorting());
                return Integer.compare(i, j);
            });
            Collections.reverse(upperTabs);
            for (int i = 0; i < upperTabs.size(); i++) {

                Tabs.add(upperTabs.get(i).getTabName());

            }
//       mExpandableListData = ExpandableListDataSource.getData(this);
//        mExpandableListTitle = new ArrayList(Tabs);
            drawerAdapter = new DrawerAdapter(this, upperTabs, this);
            recyclerView.setAdapter(drawerAdapter);

            String title = upperTabs.get(0).getTabName();
            mNavigationManager.showFragmentAction(title, String.valueOf(0), String.valueOf(0), "");
            mToolbar.setText(upperTabs.get(0).getTabName());

            getDrawerItemsCurrentDate("Refresh");
        } else {

            getDrawerItems();
        }


        if (savedInstanceState == null) {

            selectFirstItemAsDefault();

        }

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        event.getAction();/*   if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    mToolbar.setVisibility(View.VISIBLE);
                    refresh.setVisibility(View.VISIBLE);
                    search_view_lay.setVisibility(View.GONE);
                    searchView.setVisibility(View.VISIBLE);
//                    hideSoftKeyboard(MainActivity.this);
                    search_text.getText().clear();
                    Log.i("touch11","search309");
                    if(!upperTabs.isEmpty()){
                        String title = upperTabs.get(upperPosition).getTabName();
                        mNavigationManager.showFragmentAction(title, String.valueOf(subTabPosition), String.valueOf(upperPosition), "");

                    }
                }
            }*/
        return super.dispatchTouchEvent(event);
    }


    private void selectFirstItemAsDefault() {
        if (mNavigationManager != null) {

            String firstActionMovie;
            upperTabs = UpperTabs.getInstance().getSizes();


            Collections.sort(upperTabs, (fruit2, fruit1) -> {
                int i = Integer.parseInt(fruit1.getSorting());
                int j = Integer.parseInt(fruit2.getSorting());
                return Integer.compare(i, j);
            });
            Collections.reverse(upperTabs);

            if (!upperTabs.isEmpty()) {
                firstActionMovie = upperTabs.get(0).getTabName();
            } else {

                firstActionMovie = "AbOriginal";
            }

            mNavigationManager.showFragmentAction(firstActionMovie, "0", "0", "");
            Objects.requireNonNull(getSupportActionBar()).setTitle(firstActionMovie);
        }
    }

    private void getDrawerItems() {

        //  GetMethod getMethod = new GetMethod("http://aboriginal.customerdemourl.com/api/tab/get?date=1970-11-13", this, Constants.DRAWER_ITEMS_API, "loading", this);
        GetMethod getMethod = new GetMethod("https://aboriginaldev.customerdemourl.com/api/tab/get?date=1970-11-13", this, Constants.DRAWER_ITEMS_API, "loading", this);
        ApplicationController.getInstance().addToRequestQueue(getMethod.getStringRequest(), Constants.DRAWER_ITEMS_API);
    }

    private void getDrawerItemsCurrentDate(String msg) {

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());


        GetMethod getMethod = new GetMethod("https://aboriginaldev.customerdemourl.com/api/tab/get?date=" + formattedDate, this, Constants.DRAWER_ITEMS_API, msg, this);
        ApplicationController.getInstance().addToRequestQueue(getMethod.getStringRequest(), Constants.DRAWER_ITEMS_API);
    }


    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                search_text.getText().clear();
                mToolbar.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                search_view_lay.setVisibility(View.GONE);
                searchView.setVisibility(View.VISIBLE);
                // hideSoftKeyboard(MainActivity.this);
//                mToolbar.setText("AbOriginal");

                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                search_text.getText().clear();
                Log.i("touch11", "search396");
                //     hideSoftKeyboard(MainActivity.this);
//                if(!upperTabs.isEmpty())
//                mToolbar.setText(upperTabs.get(upperPosition).getTabName());


                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        EventBus.getDefault().unregister(this);
//    }
//
//
//    public void onEvent(DrawerModel drawerModel) {
//        Log.e("drawerModel",drawerModel.toString());
//
//    }

    @Override
    public void onConfigurationChanged(@NotNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResult(DrawerModel drawerModel) {


        drawerModels = new ArrayList<>(drawerModel.getResult().getIstTabs());
        Log.e("drawerModel", drawerModels.toString());


        UpperTabs.getInstance().deleteAllRecords();
        SubTabs.getInstance().deleteAllRecords();

//

//            subTabDetails.clear();
        for (int j = 0; j < drawerModels.size(); j++) {
            String size;
            if (drawerModels.get(j).getSubTabDetails().size() == 0) {
                size = "0";
            } else {
                size = "1";

            }

//              TabModel tabModel=new TabModel();
//              tabModel.setSorting(String.valueOf(drawerModels.get(j).getSorting()));
//              tabModel.setSorting(String.valueOf(drawerModels.get(j).getSorting()));
//              tabModel.setTabName(drawerModels.get(j).getTabName());
//              upperTabs.add(tabModel);
            UpperTabs.getInstance().write(drawerModels.get(j).getTabName(), drawerModels.get(j).getStaffNotes(), drawerModels.get(j).getSorting(), size);

            for (DrawerModel.Result1.IstTabs.subTabDetails subTab : drawerModels.get(j).getSubTabDetails()) {

                SubTabs.getInstance().write(String.valueOf(drawerModels.get(j).getSorting()), subTab.getSubTabName(), subTab.getDescription());
                subTabs = SubTabs.getInstance().getSizes();


            }

//              mExpandableListData.put(drawerModels.get(j).getTabName(),subTabs);
//              subTabs.add(subTab.getSubTabName());
//          }


        }
//        initItems();

        upperTabs = UpperTabs.getInstance().getSizes();


        Collections.sort(upperTabs, (fruit2, fruit1) -> {
            int i = Integer.parseInt(fruit1.getSorting());
            int j = Integer.parseInt(fruit2.getSorting());
            return Integer.compare(i, j);
        });
        Collections.reverse(upperTabs);
        for (int i = 0; i < upperTabs.size(); i++) {

            Tabs.add(upperTabs.get(i).getTabName());

        }
//       mExpandableListData = ExpandableListDataSource.getData(this);
//        mExpandableListTitle = new ArrayList(Tabs);
        drawerAdapter = new DrawerAdapter(this, upperTabs, this);
        recyclerView.setAdapter(drawerAdapter);
        setupDrawer();
//          subTabs.clear();
//
//            mExpandableListData.put(drawerModels.get(i).getTabName(), subTabs);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onResult(String textToHighlight) {

    }

    @Override
    public void onResult(int pos) {

        upperPosition = pos;
        ///recyclerView.getLayoutManager().scrollToPosition(pos);

        if (pos == 1111) {

            mDrawerLayout.closeDrawers();
        } else {

            mToolbar.setVisibility(View.VISIBLE);
            mToolbar.setText(upperTabs.get(pos).getTabName());
            mNavigationManager.showFragmentAction(upperTabs.get(pos).getTabName(), String.valueOf(0), String.valueOf(pos), "");
            search_view_lay.setVisibility(View.GONE);
            searchView.setVisibility(View.VISIBLE);
            //         recyclerView.getLayoutManager().scrollToPosition(pos);

            if (pos == 0)
                Objects.requireNonNull(recyclerView.getLayoutManager()).scrollToPosition(pos);
//                smoothScroller.setTargetPosition(pos);
//                mLayoutManager.startSmoothScroll(smoothScroller);
//            }
//            mDrawerLayout.closeDrawers();

        }

    }

    @Override
    public void onResult(int pos, String subtab, int upperPos) {


        search_text.getText().clear();
        Log.i("touch11", "search583");
        upperPosition = upperPos;
        subTabPosition = pos;
        if (subtab.equalsIgnoreCase("subTab")) {
            mDrawerLayout.closeDrawers();
            String title = upperTabs.get(upperPos).getTabName();
            mNavigationManager.showFragmentAction(title, String.valueOf(pos), String.valueOf(upperPos), "Scroll");
            mToolbar.setText(upperTabs.get(upperPos).getTabName());

//              search_view_lay.setVisibility(View.GONE);
            searchView.setVisibility(View.VISIBLE);
        }

    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);

    }

    public void getselectedservice(OnResult onResult) {

        ApiInterface apiService = ApiClient.getApi();
        Call<DrawerModel> call = apiService.getAllService();
        call.enqueue(new retrofit2.Callback<DrawerModel>() {
            @Override
            public void onResponse(retrofit2.@NotNull Call<DrawerModel> call, retrofit2.@NotNull Response<DrawerModel> response) {
                if (response.body() != null) {
                    Log.i("response", response.body().getResult().toString());
                }

                DrawerModel newsFeedModel = Utils.getGsonObject().fromJson(response.toString(), DrawerModel.class);
                Log.e("rsponseData", newsFeedModel.getResult().toString());
                onResult.onResult(newsFeedModel);
                onResult.onResult(0);
                EventBus.getDefault().post(newsFeedModel);

            }

            @Override
            public void onFailure(retrofit2.@NotNull Call<DrawerModel> call, @NotNull Throwable t) {
                Log.e("response", t.toString());

            }


        });

    }


}